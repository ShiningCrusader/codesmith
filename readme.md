# Codesmith
Tired of aligning everything yourself or using libraries that require you to override tons of properties for your site to look like you want it?

Codesmith is a mobile-first framework heavily based on flexbox that aims to minimize the code you need to write and keep customization easy.

### Engine and themes
Instead of giving you an all-in-one package with tons of styles you probably don't need, I designed Codesmith split into two halves: engine and theme.

Codesmith Engine contains everything you need to make your site work, without any significant visual style attached, while Codesmith Theme, on the other hand, needs the Engine to work and adds the essential styles to make your site look good from the get-go, without excessive frills. You'll probably need to customize your site further and I certainly don't want to hinder you in that.

Two more files, Actions and Plugins are little javascript snippets that add functionality and animation to otherwise static css components. Codesmith Actions contains the javascript required for many basic components that are found in the Engine while Codesmith Plugins contains javascript functions that add a little flair to your pages.

Reading the docs you'll find these three labels pointing out what file you need to include to use the features you're reading about.

### Responsive and adaptive
Like other frameworks before it, Codesmith is mobile-first. This means that you'll have a much easier time designing your templates and content for smaller devices and moving or adding content for larger devices.

Codesmith engine handles different devices at three screen-width breakpoints: 768px, 1200px and 1600px;

Margins and paddings are calculated from window size or font size, keeping everything visually balanced regardless of device or user zoom.
