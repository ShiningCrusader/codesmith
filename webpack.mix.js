let mix = require('laravel-mix');

mix.options({
	processCssUrls: false
});

if (mix.inProduction()) {
	mix.ts('assets/ts/codesmith-actions.ts', 'dist/js/codesmith-actions.min.js');
	mix.ts('assets/ts/codesmith-plugins.ts', 'dist/js/codesmith-plugins.min.js');
	mix.sass('assets/scss/codesmith-engine.scss', 'dist/css/codesmith-engine.min.css');
	mix.sass('assets/scss/codesmith-theme.scss', 'dist/css/codesmith-theme.min.css');
} else {
	mix.ts('assets/ts/codesmith-actions.ts', 'assets/js/codesmith-actions.js');
	mix.ts('assets/ts/codesmith-plugins.ts', 'assets/js/codesmith-plugins.js');
	mix.sass('assets/scss/codesmith-engine.scss', 'assets/css/codesmith-engine.css');
	mix.sass('assets/scss/codesmith-theme.scss', 'assets/css/codesmith-theme.css');
	mix.sass('assets/scss/tutorial.scss', 'assets/css/tutorial.css');

	mix.copy('assets/js/codesmith-actions.js', 'dist/js/codesmith-actions.js');
	mix.copy('assets/js/codesmith-plugins.js', 'dist/js/codesmith-plugins.js');
	mix.copy('assets/css/codesmith-engine.css', 'dist/css/codesmith-engine.css');
	mix.copy('assets/css/codesmith-theme.css', 'dist/css/codesmith-theme.css');
	mix.copy('assets/js/codesmith-actions.js.map', 'dist/js/codesmith-actions.js.map');
	mix.copy('assets/js/codesmith-plugins.js.map', 'dist/js/codesmith-plugins.js.map');
	mix.copy('assets/css/codesmith-engine.css.map', 'dist/css/codesmith-engine.css.map');
	mix.copy('assets/css/codesmith-theme.css.map', 'dist/css/codesmith-theme.css.map');
}

mix.setPublicPath('.');

mix.sourceMaps(true, 'source-map');
