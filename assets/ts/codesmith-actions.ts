/*jslint node: true*/
/*global $, document, window*/
'use strict';

import Navbar from './modules/navbar';
import Forms from './modules/forms';
import Togglers from './modules/togglers';
import Alerts from './modules/alerts';
import Toggables from './modules/toggables';
import Tabs from './modules/tabs';
import Modals from './modules/modals';

declare global {
	interface Window {
		Codesmith: any;
	}
}

window.Codesmith = {};

$(function() {
	$.extend(window.Codesmith, {
		// navbar
		navbar: new Navbar(),
		// checkboxes and radio
		forms: new Forms(),
		// togglers
		togglers: new Togglers(),
		//alerts
		alerts: new Alerts(),
		// toggables
		toggables: new Toggables(),
		// tabs
		tabs: new Tabs(),
		// modals
		modals: new Modals()
	});

	window.Codesmith.navbar.init();
	window.Codesmith.forms.init();
	window.Codesmith.alerts.init();
	window.Codesmith.toggables.init();
	window.Codesmith.tabs.init();
	window.Codesmith.togglers.init();
	window.Codesmith.modals.init();
});
