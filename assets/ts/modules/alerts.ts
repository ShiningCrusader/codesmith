import Config from './config';

class Alerts {
	constructor() {}

	init() {
		$(document).on('click.cs.alert', '.alert-dismiss', function(e) {
			e.preventDefault();
			let elem = $(this);
			elem.closest('.alert').slideUp(Config.ANIMATION_TIMING, function() {
				elem.trigger('close.cs.alert');
				elem.remove();
			});
		});
	}
}

export default Alerts;
