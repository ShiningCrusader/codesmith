import Config from './config';
import {Log} from './helpers';

class Tabs {
	constructor() {}

	init() {
		let self = this;
		$('[data-tab]').each(function() {
			let tab = self.getTab($(this));
			if (tab.hasClass('open')) {
				self.update($(this));
			}
		});
		$(document).on('click.cs.tabs', '[data-tab]', function(e) {
			e.preventDefault();
			let elem = $(this);
			if (!elem.hasClass('active')) {
				self.update(elem);
			}
		});
	}

	getTab(elem: JQuery) {
		let tab = elem.data('tab');
		if (!tab) {
			Log.error('Tabs - Tab selector missing for tab trigger');
		}
		return $(tab);
	}

	getParent(elem: JQuery) {
		let parent = elem.data('parent');
		if (!parent) {
			Log.error('Tabs - Parent selector missing for tab trigger');
		}
		return $(parent);
	}

	update(elem: JQuery) {
		let self = this;
		let parent = elem.data('parent');
		let old_tab: JQuery;
		$('[data-parent="' + parent + '"]').each(function() {
			let tab = self.getTab($(this));
			if (tab.hasClass('open')) {
				old_tab = tab;
			}
		});
		let new_tab = self.getTab(elem);

		new_tab.trigger('toggle.cs.tab');
		old_tab.trigger('toggle.cs.tab');

		if ($(parent).hasClass('fade')) {
			old_tab.trigger('hide.cs.tab');
			old_tab.stop(true).fadeOut(Config.ANIMATION_TIMING, function() {
				old_tab.removeClass('open');
				new_tab
					.addClass('open')
					.stop(true)
					.fadeIn(Config.ANIMATION_TIMING);
				new_tab.trigger('show.cs.tab');
			});
		} else {
			new_tab.trigger('show.cs.tab');
			old_tab.trigger('hide.cs.tab');
			old_tab.removeClass('open').hide();
			new_tab.addClass('open').show();
		}
	}
}

export default Tabs;
