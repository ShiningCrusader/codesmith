declare global {
	interface Window {
		SIZE_M: number;
		SIZE_L: number;
		SIZE_H: number;
		ANIMATION_TIMING: number;
		NAVBAR_TRIGGER: number;
	}
}

export default {
	SIZE_M: window.SIZE_M || 768,
	SIZE_L: window.SIZE_L || 1200,
	SIZE_H: window.SIZE_H || 1600,

	ANIMATION_TIMING: window.ANIMATION_TIMING || 200,

	NAVBAR_TRIGGER: window.NAVBAR_TRIGGER || window.SIZE_M || 768,

	LOG_HEAD: 'Codesmith'
};
