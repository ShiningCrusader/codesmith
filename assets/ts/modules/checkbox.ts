class Checkbox {
	private selector = 'input[type="checkbox"], input[type="radio"]';

	constructor() {}

	init() {
		let self = this;
		$(document).on('change.cs.checkbox', this.selector, function() {
			let elem = $(this);
			if (elem.is('input[type="radio"]')) {
				$('input[type="radio"][name="' + elem.attr('name') + '"]').each(function() {
					self.update($(this));
				});
			} else {
				self.update(elem);
			}
		});
		$(document).on('focus.cs.checkbox', this.selector, function() {
			let elem = $(this);
			elem.closest('.form-checkbox, .form-radio').addClass('has-focus');
		});
		$(document).on('blur.cs.checkbox', this.selector, function() {
			let elem = $(this);
			elem.closest('.form-checkbox, .form-radio').removeClass('has-focus');
		});
		$(this.selector).each(function(_i, elem) {
			$(elem).trigger('change.cs.checkbox');
		});
	}

	update(elem: JQuery) {
		if (elem.is(':checked')) {
			elem.closest('.form-checkbox, .form-radio').addClass('checked');
		} else {
			elem.closest('.form-checkbox, .form-radio').removeClass('checked');
		}
	}
}

export default Checkbox;
