import Checkbox from './checkbox';
import File from './file';

class Forms {
	private checkbox: Checkbox;
	private file: File;

	constructor() {
		this.checkbox = new Checkbox();
		this.file = new File();
	}

	init() {
		this.checkbox.init();
		this.file.init();
	}
}

export default Forms;
