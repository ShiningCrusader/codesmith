import Config from './config';
import {Query} from './helpers';

class Navbar {
	private navs: JQuery;

	constructor() {}

	init() {
		this.navs = $('.nav');
		let self = this;
		this.navs.on('click.cs.navbar', function(e) {
			e.stopPropagation();
		});
		$('.nav-toggle').on('click.cs.navbar', (e) => {
			e.preventDefault();
			e.stopPropagation();
			let elem = $(e.currentTarget);
			let target_nav = $(elem.data('nav'));

			target_nav.toggleClass('open');
			if (!target_nav.hasClass('vertical')) {
				target_nav.slideToggle(Config.ANIMATION_TIMING);
			}

			// events
			target_nav.trigger('toggle.cs.nav');
			if (target_nav.hasClass('open')) {
				target_nav.trigger('open.cs.nav');
			} else {
				target_nav.trigger('close.cs.nav');
			}
		});
		$('.nav-sub-toggle').on('click.cs.navbar', function(e) {
			e.preventDefault();
			e.stopPropagation();

			let nav_sub = $(this).siblings('.nav-sub');
			let other_subs = $(this)
				.closest('.nav-item')
				.siblings()
				.find('.nav-sub');

			other_subs.removeClass('open').trigger('close.cs.nav-sub');
			nav_sub.toggleClass('open');

			// events
			nav_sub.trigger('toggle.cs.nav-sub');
			if (nav_sub.hasClass('open')) nav_sub.trigger('open.cs.nav-sub');
			else nav_sub.trigger('close.cs.nav-sub');
		});
		$('html').on('click.cs.navbar', () => {
			$('.nav-sub-toggle')
				.siblings('.nav-sub')
				.removeClass('open');
			self.navs.removeClass('open');
			if (!Query.navbar) {
				self.navs.each(function() {
					let elem = $(this);
					if (!elem.hasClass('vertical')) {
						elem.slideUp(Config.ANIMATION_TIMING);
					}
				});
				// events
				self.navs.trigger('close.cs.nav');
			}
		});
	}
}

export default Navbar;
