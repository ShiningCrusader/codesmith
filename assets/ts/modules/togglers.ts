class Togglers {
	constructor() {}

	init() {
		$(document).on('click.togglers', '[data-toggle]', function(e) {
			e.preventDefault();
			let elem = $(this);
			let toggle_class = elem.data('toggle');
			if (toggle_class.indexOf('.') >= 0) {
				toggle_class = toggle_class.replace(/\./g, ' ').trim();
			}

			let toggle_target: any;
			if (elem.attr('href')) {
				toggle_target = elem.attr('href');
			} else if (elem.data('target')) {
				toggle_target = elem.data('target');
			} else {
				toggle_target = elem;
			}

			$(toggle_target).toggleClass(toggle_class);
			$(toggle_target).trigger('toggle.cs.toggable');
		});
	}
}

export default Togglers;
