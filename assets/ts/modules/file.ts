class File {
	private selector = 'input[type="file"]';

	constructor() {}

	init() {
		let self = this;
		$(document).on('change.cs.file', self.selector, function() {
			let elem = $(this),
				label = elem.siblings('label').first();
			if (elem.data('label') == undefined) {
				elem.data('label', label.html());
			}
			self.update(elem, label);
		});
		$(document).on('focus.cs.file', self.selector, function() {
			let elem = $(this);
			elem.addClass('has-focus');
		});
		$(document).on('blur.cs.file', self.selector, function() {
			let elem = $(this);
			elem.removeClass('has-focus');
		});
	}

	update(elem: JQuery<any>, label: JQuery) {
		let fileName = '';
		let files = elem[0].files;
		if (files && files.length > 1) fileName = (elem.data('selected-caption') || '').replace('{count}', files.length);
		else fileName = (elem.val() as string).split('\\').pop();

		if (fileName) label.html(fileName);
		else label.html(elem.data('label'));
	}
}

export default File;
