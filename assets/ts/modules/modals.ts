import {Log} from './helpers';

class ModalButton {
	constructor(parent: string, action: string, text: string, classes = '', handler = function() {}) {
		let button = $('<a></a>');
		button.html(text);
		button.addClass('btn');
		if (classes) {
			button.addClass(classes);
		}
		switch (action) {
			case 'close':
				button.attr('href', parent).attr('data-modal-close', '');
				break;
			case 'function':
				if (handler) {
					button.on('click.cs.modalbutton', function() {
						handler();
					});
				} else {
					Log.error('ModalButton - Missing handler for button');
				}
				break;
		}
		return button;
	}
}

class Modals {
	private selector: string;
	private elem: JQuery;
	private defaults: object;
	private background: JQuery;

	constructor(selector: string = '') {
		this.selector = selector;
		if (selector) {
			this.elem = $(selector);
		}
		this.defaults = {
			header: '',
			body: '',
			footer: '',
			fade: true,
			actions: []
		};
	}

	open(): void {
		this.createBackground();
		if (this.elem.hasClass('fade')) {
			this.elem
				.fadeIn(() => {
					this.elem.addClass('open');
					this.elem.removeAttr('style');
				})
				.css('display', 'flex');
		} else {
			this.elem.addClass('open');
		}
		this.elem.trigger('open.cs.modal');
	}

	close(): void {
		this.destroyBackground();
		if (this.elem.hasClass('fade')) {
			this.elem
				.fadeOut(() => {
					this.elem.removeClass('open');
				})
				.css('display', 'flex');
		} else {
			this.elem.removeClass('open');
		}
		this.elem.trigger('close.cs.modal');
	}

	createBackground(): void {
		this.background = $('<div></div>')
			.addClass('modal-background')
			.attr('data-modal-close', this.selector);
		if (this.elem.hasClass('fade')) {
			this.background.addClass('fade');
		}
		this.background.on('click.cs.modals', () => {
			this.close();
		});
		this.background.appendTo('body');
		if (this.background.hasClass('fade')) {
			this.background.fadeIn();
		} else {
			this.background.show();
		}
	}

	destroyBackground(): void {
		if (this.background) {
			if (this.background.hasClass('fade')) {
				this.background.fadeOut(() => {
					this.background.remove();
					this.background = undefined;
				});
			} else {
				this.background.remove();
				this.background = undefined;
			}
		} else {
			let background = $('.modal-background[data-modal-close="' + this.selector + '"]');
			if (background.hasClass('fade')) {
				background.fadeOut(() => {
					background.remove();
				});
			} else {
				background.remove();
			}
		}
	}

	setup(selector: string): Modals {
		return new Modals(selector);
	}

	create(options: any): Modals {
		let id = 'modal-' + Math.floor(Math.random() * 10000);

		options = $.extend({}, this.defaults, options);

		let modal = $('<div></div>')
			.attr('id', id)
			.addClass('modal');
		if (options.fade) {
			modal.addClass('fade');
		}
		if (options.header) {
			let header = $('<div></div>')
				.addClass('modal-header')
				.html(options.header);
			modal.append(header);
		}
		if (options.body) {
			let body = $('<div></div>')
				.addClass('modal-body')
				.html(options.body);
			modal.append(body);
		}
		if (options.footer || options.actions) {
			let footer = $('<div></div>').addClass('modal-footer');
			if (options.footer) {
				footer.html(options.footer);
			}
			if (options.actions) {
				let actions = $('<div></div>').addClass('modal-actions');
				for (let _action of options.actions) {
					let button = new ModalButton('#' + id, _action.action, _action.text, _action.classes, _action.handler);
					actions.append(button as JQuery);
				}
				footer.append(actions);
			}
			modal.append(footer);
		}

		modal.appendTo('body');
		return this.setup('#' + id);
	}

	init() {
		let self = this;
		$(document).on('click.cs.modals', '[data-modal]', function(e) {
			e.preventDefault();
			let elem = $(this);
			let target = elem.attr('href') || elem.data('modal');
			if (target) {
				let modal = self.setup(target);
				modal.open();
			}
		});
		$(document).on('click.cs.modals', '[data-modal-close]', function(e) {
			e.preventDefault();
			let elem = $(this);
			let target = elem.attr('href') || elem.data('modal-close');
			if (target) {
				let modal = new Modals(target);
				modal.close();
			}
		});
	}
}

export default Modals;
