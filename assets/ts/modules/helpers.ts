import Config from './config';

class Vec2 {
	public x: number;
	public y: number;

	constructor(x = 0, y = 0) {
		this.x = x;
		this.y = y;
	}

	length() {
		return Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2));
	}

	normalize() {
		let length = this.length();
		this.x /= length;
		this.y /= length;
	}
}

class Delay {
	static get STATE_PAUSED() {
		return 0;
	}
	static get STATE_PLAYING() {
		return 1;
	}

	private context: any;
	private callback: () => void;
	private retrigger: boolean;
	private time: number = null;
	private timeout: number;
	private state: number;

	constructor(context: any, callback: () => void, time: number, retrigger: boolean = false) {
		this.context = context;
		this.callback = callback;
		this.retrigger = retrigger;
		this.time = time;
		this.state = Delay.STATE_PAUSED;
	}

	start() {
		if (!this.isPlaying || this.retrigger) {
			this.state = Delay.STATE_PLAYING;
			this.timeout = window.setTimeout(() => {
				this.state = Delay.STATE_PAUSED;
				this.callback.call(this.context);
			}, this.time);
		}
	}

	stop() {
		this.state = Delay.STATE_PAUSED;
		clearTimeout(this.timeout);
	}

	isPlaying() {
		return this.state;
	}
}

class Window {
	static get size() {
		let win = $(window);
		return new Vec2(win.innerWidth(), win.innerHeight());
	}

	static get scroll() {
		return $(window).scrollTop();
	}

	constructor() {}
}

class Query {
	constructor() {}

	static query(size: number) {
		return window.matchMedia('(min-width: ' + size + 'px)');
	}
	static get query_m() {
		return Query.query(Config.SIZE_M).matches;
	}
	static get query_l() {
		return Query.query(Config.SIZE_L).matches;
	}
	static get query_h() {
		return Query.query(Config.SIZE_H).matches;
	}
	static get navbar() {
		return Query.query(Config.NAVBAR_TRIGGER).matches;
	}
}

class Log {
	constructor() {}

	static log(text: string) {
		console.log(Config.LOG_HEAD + ' - ' + text);
	}
	static warn(text: string) {
		console.warn(Config.LOG_HEAD + ' - ' + text);
	}
	static error(text: string) {
		console.log(Config.LOG_HEAD + ' - ' + text);
	}
}

export {Vec2, Delay, Window, Query, Log};
