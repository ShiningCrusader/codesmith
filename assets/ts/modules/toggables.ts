import Config from './config';

class Toggables {
	constructor() {}

	init() {
		let self = this;
		$(document).on('click.cs.toggables', '[data-toggable]', function(e) {
			e.preventDefault();
			let elem = $(this);
			let parent = elem.data('parent');
			if (parent) {
				$('[data-parent="' + parent + '"]')
					.not(elem)
					.each(function() {
						self.toggableClose($(this));
					});
			}
			self.toggableToggle(elem);
		});
	}

	toggableToggle(elem: JQuery) {
		let self = this;
		let toggable = $(elem.data('toggable'));
		toggable.toggleClass('open');
		self.update(toggable);
	}

	toggableClose(elem: JQuery) {
		let self = this;
		let toggable = $(elem.data('toggable'));
		toggable.removeClass('open');
		self.update(toggable);
	}

	update(target: JQuery) {
		target.trigger('toggle.cs.toggable');
		if (target.hasClass('open')) {
			target.trigger('open.cs.toggable');
			target.slideDown(Config.ANIMATION_TIMING, function() {
				target.trigger('opened.cs.toggable');
			});
		} else {
			target.trigger('toggle.cs.toggable');
			target.trigger('close.cs.toggable');
			target.slideUp(Config.ANIMATION_TIMING, function() {
				target.trigger('closed.cs.toggable');
			});
		}
	}
}

export default Toggables;
