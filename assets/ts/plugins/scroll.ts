import Config from '../modules/config';

class Scroll {
	constructor() {}

	init() {
		$(document).on('click', '[data-scroll]', function(e) {
			e.preventDefault();
			let elem = $(this);
			let target = $(elem.data('scroll'));
			let target_top = target.offset().top;
			let scroll_position;
			if (elem.data('no-header')) scroll_position = target_top;
			else {
				scroll_position = target_top - $('.header').outerHeight();
			}
			target.trigger('start.cs.scroll');
			$('html, body')
				.stop()
				.animate({scrollTop: scroll_position}, Config.ANIMATION_TIMING * 2.5, function() {
					target.trigger('finish.cs.scroll');
				});
		});
	}
}

export default Scroll;
