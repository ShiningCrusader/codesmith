class Parallax {
	constructor() {}

	init() {
		$(window).on('load.cs.parallax scroll.cs.parallax resize.cs.parallax', () => {
			this.update();
		});
		this.update();
	}

	update() {
		let win_top = $(window).scrollTop();
		let win_height = $(window).height();
		$('[data-parallax]').each(function() {
			let elem = $(this);
			if (parseFloat(elem.data('parallax')) > 0) {
				let elem_height = elem.outerHeight();
				let elem_bottom = elem.offset().top + elem_height;
				let scroll_percentage = ((elem_bottom - win_top) / (win_height + elem_height)) * 100;
				let position = 50 + (scroll_percentage - 50) * elem.data('parallax');
				elem.css('background-position-y', position + '%');
				elem.css('background-position-x', '50%');
			}
		});
	}
}

export default Parallax;
