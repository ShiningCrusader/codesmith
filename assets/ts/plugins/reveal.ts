import {Delay, Window, Log} from '../modules/helpers';

class Reveal {
	private targets: JQuery;
	private defaults: Object;
	private threshold: number;
	private delay: Delay;
	private event: Function;

	constructor() {
		this.defaults = {
			threshold: -50,
			delay: 150
		};
	}

	init(options: any) {
		options = {...this.defaults, ...options};
		if (!options.selector) {
			Log.error('Reveal - You need to provide a jQuery selector');
		} else {
			this.targets = $(options.selector);
			this.threshold = options.threshold;
			this.delay = new Delay(
				this,
				function() {
					this.trigger();
				},
				options.delay,
				true
			);
			this.targets.css('animation-name', 'none');
			this.trigger();
			$(window).on('scroll.cs.reveal', () => {
				this.check();
			});
		}
	}

	check() {
		this.delay.start();
	}

	trigger() {
		let scroll = Window.scroll;
		let temp_targets = this.targets;
		let self = this;

		this.targets.each(function() {
			let elem = $(this);
			let elem_top = elem.offset().top;
			let elem_bottom = elem_top + elem.outerHeight();
			if (elem_top < scroll + Window.size.y + self.threshold && elem_bottom > scroll - self.threshold) {
				elem.attr('style', '');
				elem.css('visibility', 'visible');
				temp_targets = temp_targets.not(elem);
			}
		});

		this.targets = temp_targets;

		if (!this.targets.length) {
			$(window).off('scroll.cs.reveal');
		}
	}
}

export default Reveal;
