import Parallax from './plugins/parallax';
import Scroll from './plugins/scroll';
import Reveal from './plugins/reveal';

declare global {
	interface Window {
		Codesmith: any;
	}
}

$(function() {
	$.extend(window.Codesmith, {
		// parallax backgrounds
		parallax: new Parallax(),
		// scoll to
		scroll: new Scroll(),
		// reveal
		reveal: new Reveal()
	});

	window.Codesmith.scroll.init();
	window.Codesmith.parallax.init();
});
