/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./assets/scss/codesmith-engine.scss":
/*!*******************************************!*\
  !*** ./assets/scss/codesmith-engine.scss ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "./assets/scss/codesmith-theme.scss":
/*!******************************************!*\
  !*** ./assets/scss/codesmith-theme.scss ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "./assets/scss/tutorial.scss":
/*!***********************************!*\
  !*** ./assets/scss/tutorial.scss ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "./assets/ts/codesmith-actions.ts":
/*!****************************************!*\
  !*** ./assets/ts/codesmith-actions.ts ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const navbar_1 = __webpack_require__(/*! ./modules/navbar */ "./assets/ts/modules/navbar.ts");
const forms_1 = __webpack_require__(/*! ./modules/forms */ "./assets/ts/modules/forms.ts");
const togglers_1 = __webpack_require__(/*! ./modules/togglers */ "./assets/ts/modules/togglers.ts");
const alerts_1 = __webpack_require__(/*! ./modules/alerts */ "./assets/ts/modules/alerts.ts");
const toggables_1 = __webpack_require__(/*! ./modules/toggables */ "./assets/ts/modules/toggables.ts");
const tabs_1 = __webpack_require__(/*! ./modules/tabs */ "./assets/ts/modules/tabs.ts");
const modals_1 = __webpack_require__(/*! ./modules/modals */ "./assets/ts/modules/modals.ts");
window.Codesmith = {};
$(function () {
    $.extend(window.Codesmith, {
        navbar: new navbar_1.default(),
        forms: new forms_1.default(),
        togglers: new togglers_1.default(),
        alerts: new alerts_1.default(),
        toggables: new toggables_1.default(),
        tabs: new tabs_1.default(),
        modals: new modals_1.default()
    });
    window.Codesmith.navbar.init();
    window.Codesmith.forms.init();
    window.Codesmith.alerts.init();
    window.Codesmith.toggables.init();
    window.Codesmith.tabs.init();
    window.Codesmith.togglers.init();
    window.Codesmith.modals.init();
});


/***/ }),

/***/ "./assets/ts/modules/alerts.ts":
/*!*************************************!*\
  !*** ./assets/ts/modules/alerts.ts ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = __webpack_require__(/*! ./config */ "./assets/ts/modules/config.ts");
class Alerts {
    constructor() { }
    init() {
        $(document).on('click.cs.alert', '.alert-dismiss', function (e) {
            e.preventDefault();
            let elem = $(this);
            elem.closest('.alert').slideUp(config_1.default.ANIMATION_TIMING, function () {
                elem.trigger('close.cs.alert');
                elem.remove();
            });
        });
    }
}
exports.default = Alerts;


/***/ }),

/***/ "./assets/ts/modules/checkbox.ts":
/*!***************************************!*\
  !*** ./assets/ts/modules/checkbox.ts ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
class Checkbox {
    constructor() {
        this.selector = 'input[type="checkbox"], input[type="radio"]';
    }
    init() {
        let self = this;
        $(document).on('change.cs.checkbox', this.selector, function () {
            let elem = $(this);
            if (elem.is('input[type="radio"]')) {
                $('input[type="radio"][name="' + elem.attr('name') + '"]').each(function () {
                    self.update($(this));
                });
            }
            else {
                self.update(elem);
            }
        });
        $(document).on('focus.cs.checkbox', this.selector, function () {
            let elem = $(this);
            elem.closest('.form-checkbox, .form-radio').addClass('has-focus');
        });
        $(document).on('blur.cs.checkbox', this.selector, function () {
            let elem = $(this);
            elem.closest('.form-checkbox, .form-radio').removeClass('has-focus');
        });
        $(this.selector).each(function (_i, elem) {
            $(elem).trigger('change.cs.checkbox');
        });
    }
    update(elem) {
        if (elem.is(':checked')) {
            elem.closest('.form-checkbox, .form-radio').addClass('checked');
        }
        else {
            elem.closest('.form-checkbox, .form-radio').removeClass('checked');
        }
    }
}
exports.default = Checkbox;


/***/ }),

/***/ "./assets/ts/modules/config.ts":
/*!*************************************!*\
  !*** ./assets/ts/modules/config.ts ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.default = {
    SIZE_M: window.SIZE_M || 768,
    SIZE_L: window.SIZE_L || 1200,
    SIZE_H: window.SIZE_H || 1600,
    ANIMATION_TIMING: window.ANIMATION_TIMING || 200,
    NAVBAR_TRIGGER: window.NAVBAR_TRIGGER || window.SIZE_M || 768,
    LOG_HEAD: 'Codesmith'
};


/***/ }),

/***/ "./assets/ts/modules/file.ts":
/*!***********************************!*\
  !*** ./assets/ts/modules/file.ts ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
class File {
    constructor() {
        this.selector = 'input[type="file"]';
    }
    init() {
        let self = this;
        $(document).on('change.cs.file', self.selector, function () {
            let elem = $(this), label = elem.siblings('label').first();
            if (elem.data('label') == undefined) {
                elem.data('label', label.html());
            }
            self.update(elem, label);
        });
        $(document).on('focus.cs.file', self.selector, function () {
            let elem = $(this);
            elem.addClass('has-focus');
        });
        $(document).on('blur.cs.file', self.selector, function () {
            let elem = $(this);
            elem.removeClass('has-focus');
        });
    }
    update(elem, label) {
        let fileName = '';
        let files = elem[0].files;
        if (files && files.length > 1)
            fileName = (elem.data('selected-caption') || '').replace('{count}', files.length);
        else
            fileName = elem.val().split('\\').pop();
        if (fileName)
            label.html(fileName);
        else
            label.html(elem.data('label'));
    }
}
exports.default = File;


/***/ }),

/***/ "./assets/ts/modules/forms.ts":
/*!************************************!*\
  !*** ./assets/ts/modules/forms.ts ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const checkbox_1 = __webpack_require__(/*! ./checkbox */ "./assets/ts/modules/checkbox.ts");
const file_1 = __webpack_require__(/*! ./file */ "./assets/ts/modules/file.ts");
class Forms {
    constructor() {
        this.checkbox = new checkbox_1.default();
        this.file = new file_1.default();
    }
    init() {
        this.checkbox.init();
        this.file.init();
    }
}
exports.default = Forms;


/***/ }),

/***/ "./assets/ts/modules/helpers.ts":
/*!**************************************!*\
  !*** ./assets/ts/modules/helpers.ts ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = __webpack_require__(/*! ./config */ "./assets/ts/modules/config.ts");
class Vec2 {
    constructor(x = 0, y = 0) {
        this.x = x;
        this.y = y;
    }
    length() {
        return Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2));
    }
    normalize() {
        let length = this.length();
        this.x /= length;
        this.y /= length;
    }
}
exports.Vec2 = Vec2;
class Delay {
    constructor(context, callback, time, retrigger = false) {
        this.time = null;
        this.context = context;
        this.callback = callback;
        this.retrigger = retrigger;
        this.time = time;
        this.state = Delay.STATE_PAUSED;
    }
    static get STATE_PAUSED() {
        return 0;
    }
    static get STATE_PLAYING() {
        return 1;
    }
    start() {
        if (!this.isPlaying || this.retrigger) {
            this.state = Delay.STATE_PLAYING;
            this.timeout = window.setTimeout(() => {
                this.state = Delay.STATE_PAUSED;
                this.callback.call(this.context);
            }, this.time);
        }
    }
    stop() {
        this.state = Delay.STATE_PAUSED;
        clearTimeout(this.timeout);
    }
    isPlaying() {
        return this.state;
    }
}
exports.Delay = Delay;
class Window {
    static get size() {
        let win = $(window);
        return new Vec2(win.innerWidth(), win.innerHeight());
    }
    static get scroll() {
        return $(window).scrollTop();
    }
    constructor() { }
}
exports.Window = Window;
class Query {
    constructor() { }
    static query(size) {
        return window.matchMedia('(min-width: ' + size + 'px)');
    }
    static get query_m() {
        return Query.query(config_1.default.SIZE_M).matches;
    }
    static get query_l() {
        return Query.query(config_1.default.SIZE_L).matches;
    }
    static get query_h() {
        return Query.query(config_1.default.SIZE_H).matches;
    }
    static get navbar() {
        return Query.query(config_1.default.NAVBAR_TRIGGER).matches;
    }
}
exports.Query = Query;
class Log {
    constructor() { }
    static log(text) {
        console.log(config_1.default.LOG_HEAD + ' - ' + text);
    }
    static warn(text) {
        console.warn(config_1.default.LOG_HEAD + ' - ' + text);
    }
    static error(text) {
        console.log(config_1.default.LOG_HEAD + ' - ' + text);
    }
}
exports.Log = Log;


/***/ }),

/***/ "./assets/ts/modules/modals.ts":
/*!*************************************!*\
  !*** ./assets/ts/modules/modals.ts ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const helpers_1 = __webpack_require__(/*! ./helpers */ "./assets/ts/modules/helpers.ts");
class ModalButton {
    constructor(parent, action, text, classes = '', handler = function () { }) {
        let button = $('<a></a>');
        button.html(text);
        button.addClass('btn');
        if (classes) {
            button.addClass(classes);
        }
        switch (action) {
            case 'close':
                button.attr('href', parent).attr('data-modal-close', '');
                break;
            case 'function':
                if (handler) {
                    button.on('click.cs.modalbutton', function () {
                        handler();
                    });
                }
                else {
                    helpers_1.Log.error('ModalButton - Missing handler for button');
                }
                break;
        }
        return button;
    }
}
class Modals {
    constructor(selector = '') {
        this.selector = selector;
        if (selector) {
            this.elem = $(selector);
        }
        this.defaults = {
            header: '',
            body: '',
            footer: '',
            fade: true,
            actions: []
        };
    }
    open() {
        this.createBackground();
        if (this.elem.hasClass('fade')) {
            this.elem
                .fadeIn(() => {
                this.elem.addClass('open');
                this.elem.removeAttr('style');
            })
                .css('display', 'flex');
        }
        else {
            this.elem.addClass('open');
        }
        this.elem.trigger('open.cs.modal');
    }
    close() {
        this.destroyBackground();
        if (this.elem.hasClass('fade')) {
            this.elem
                .fadeOut(() => {
                this.elem.removeClass('open');
            })
                .css('display', 'flex');
        }
        else {
            this.elem.removeClass('open');
        }
        this.elem.trigger('close.cs.modal');
    }
    createBackground() {
        this.background = $('<div></div>')
            .addClass('modal-background')
            .attr('data-modal-close', this.selector);
        if (this.elem.hasClass('fade')) {
            this.background.addClass('fade');
        }
        this.background.on('click.cs.modals', () => {
            this.close();
        });
        this.background.appendTo('body');
        if (this.background.hasClass('fade')) {
            this.background.fadeIn();
        }
        else {
            this.background.show();
        }
    }
    destroyBackground() {
        if (this.background) {
            if (this.background.hasClass('fade')) {
                this.background.fadeOut(() => {
                    this.background.remove();
                    this.background = undefined;
                });
            }
            else {
                this.background.remove();
                this.background = undefined;
            }
        }
        else {
            let background = $('.modal-background[data-modal-close="' + this.selector + '"]');
            if (background.hasClass('fade')) {
                background.fadeOut(() => {
                    background.remove();
                });
            }
            else {
                background.remove();
            }
        }
    }
    setup(selector) {
        return new Modals(selector);
    }
    create(options) {
        let id = 'modal-' + Math.floor(Math.random() * 10000);
        options = $.extend({}, this.defaults, options);
        let modal = $('<div></div>')
            .attr('id', id)
            .addClass('modal');
        if (options.fade) {
            modal.addClass('fade');
        }
        if (options.header) {
            let header = $('<div></div>')
                .addClass('modal-header')
                .html(options.header);
            modal.append(header);
        }
        if (options.body) {
            let body = $('<div></div>')
                .addClass('modal-body')
                .html(options.body);
            modal.append(body);
        }
        if (options.footer || options.actions) {
            let footer = $('<div></div>').addClass('modal-footer');
            if (options.footer) {
                footer.html(options.footer);
            }
            if (options.actions) {
                let actions = $('<div></div>').addClass('modal-actions');
                for (let _action of options.actions) {
                    let button = new ModalButton('#' + id, _action.action, _action.text, _action.classes, _action.handler);
                    actions.append(button);
                }
                footer.append(actions);
            }
            modal.append(footer);
        }
        modal.appendTo('body');
        return this.setup('#' + id);
    }
    init() {
        let self = this;
        $(document).on('click.cs.modals', '[data-modal]', function (e) {
            e.preventDefault();
            let elem = $(this);
            let target = elem.attr('href') || elem.data('modal');
            if (target) {
                let modal = self.setup(target);
                modal.open();
            }
        });
        $(document).on('click.cs.modals', '[data-modal-close]', function (e) {
            e.preventDefault();
            let elem = $(this);
            let target = elem.attr('href') || elem.data('modal-close');
            if (target) {
                let modal = new Modals(target);
                modal.close();
            }
        });
    }
}
exports.default = Modals;


/***/ }),

/***/ "./assets/ts/modules/navbar.ts":
/*!*************************************!*\
  !*** ./assets/ts/modules/navbar.ts ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = __webpack_require__(/*! ./config */ "./assets/ts/modules/config.ts");
const helpers_1 = __webpack_require__(/*! ./helpers */ "./assets/ts/modules/helpers.ts");
class Navbar {
    constructor() { }
    init() {
        this.navs = $('.nav');
        let self = this;
        this.navs.on('click.cs.navbar', function (e) {
            e.stopPropagation();
        });
        $('.nav-toggle').on('click.cs.navbar', (e) => {
            e.preventDefault();
            e.stopPropagation();
            let elem = $(e.currentTarget);
            let target_nav = $(elem.data('nav'));
            target_nav.toggleClass('open');
            if (!target_nav.hasClass('vertical')) {
                target_nav.slideToggle(config_1.default.ANIMATION_TIMING);
            }
            target_nav.trigger('toggle.cs.nav');
            if (target_nav.hasClass('open')) {
                target_nav.trigger('open.cs.nav');
            }
            else {
                target_nav.trigger('close.cs.nav');
            }
        });
        $('.nav-sub-toggle').on('click.cs.navbar', function (e) {
            e.preventDefault();
            e.stopPropagation();
            let nav_sub = $(this).siblings('.nav-sub');
            let other_subs = $(this)
                .closest('.nav-item')
                .siblings()
                .find('.nav-sub');
            other_subs.removeClass('open').trigger('close.cs.nav-sub');
            nav_sub.toggleClass('open');
            nav_sub.trigger('toggle.cs.nav-sub');
            if (nav_sub.hasClass('open'))
                nav_sub.trigger('open.cs.nav-sub');
            else
                nav_sub.trigger('close.cs.nav-sub');
        });
        $('html').on('click.cs.navbar', () => {
            $('.nav-sub-toggle')
                .siblings('.nav-sub')
                .removeClass('open');
            self.navs.removeClass('open');
            if (!helpers_1.Query.navbar) {
                self.navs.each(function () {
                    let elem = $(this);
                    if (!elem.hasClass('vertical')) {
                        elem.slideUp(config_1.default.ANIMATION_TIMING);
                    }
                });
                self.navs.trigger('close.cs.nav');
            }
        });
    }
}
exports.default = Navbar;


/***/ }),

/***/ "./assets/ts/modules/tabs.ts":
/*!***********************************!*\
  !*** ./assets/ts/modules/tabs.ts ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = __webpack_require__(/*! ./config */ "./assets/ts/modules/config.ts");
const helpers_1 = __webpack_require__(/*! ./helpers */ "./assets/ts/modules/helpers.ts");
class Tabs {
    constructor() { }
    init() {
        let self = this;
        $('[data-tab]').each(function () {
            let tab = self.getTab($(this));
            if (tab.hasClass('open')) {
                self.update($(this));
            }
        });
        $(document).on('click.cs.tabs', '[data-tab]', function (e) {
            e.preventDefault();
            let elem = $(this);
            if (!elem.hasClass('active')) {
                self.update(elem);
            }
        });
    }
    getTab(elem) {
        let tab = elem.data('tab');
        if (!tab) {
            helpers_1.Log.error('Tabs - Tab selector missing for tab trigger');
        }
        return $(tab);
    }
    getParent(elem) {
        let parent = elem.data('parent');
        if (!parent) {
            helpers_1.Log.error('Tabs - Parent selector missing for tab trigger');
        }
        return $(parent);
    }
    update(elem) {
        let self = this;
        let parent = elem.data('parent');
        let old_tab;
        $('[data-parent="' + parent + '"]').each(function () {
            let tab = self.getTab($(this));
            if (tab.hasClass('open')) {
                old_tab = tab;
            }
        });
        let new_tab = self.getTab(elem);
        new_tab.trigger('toggle.cs.tab');
        old_tab.trigger('toggle.cs.tab');
        if ($(parent).hasClass('fade')) {
            old_tab.trigger('hide.cs.tab');
            old_tab.stop(true).fadeOut(config_1.default.ANIMATION_TIMING, function () {
                old_tab.removeClass('open');
                new_tab
                    .addClass('open')
                    .stop(true)
                    .fadeIn(config_1.default.ANIMATION_TIMING);
                new_tab.trigger('show.cs.tab');
            });
        }
        else {
            new_tab.trigger('show.cs.tab');
            old_tab.trigger('hide.cs.tab');
            old_tab.removeClass('open').hide();
            new_tab.addClass('open').show();
        }
    }
}
exports.default = Tabs;


/***/ }),

/***/ "./assets/ts/modules/toggables.ts":
/*!****************************************!*\
  !*** ./assets/ts/modules/toggables.ts ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = __webpack_require__(/*! ./config */ "./assets/ts/modules/config.ts");
class Toggables {
    constructor() { }
    init() {
        let self = this;
        $(document).on('click.cs.toggables', '[data-toggable]', function (e) {
            e.preventDefault();
            let elem = $(this);
            let parent = elem.data('parent');
            if (parent) {
                $('[data-parent="' + parent + '"]')
                    .not(elem)
                    .each(function () {
                    self.toggableClose($(this));
                });
            }
            self.toggableToggle(elem);
        });
    }
    toggableToggle(elem) {
        let self = this;
        let toggable = $(elem.data('toggable'));
        toggable.toggleClass('open');
        self.update(toggable);
    }
    toggableClose(elem) {
        let self = this;
        let toggable = $(elem.data('toggable'));
        toggable.removeClass('open');
        self.update(toggable);
    }
    update(target) {
        target.trigger('toggle.cs.toggable');
        if (target.hasClass('open')) {
            target.trigger('open.cs.toggable');
            target.slideDown(config_1.default.ANIMATION_TIMING, function () {
                target.trigger('opened.cs.toggable');
            });
        }
        else {
            target.trigger('toggle.cs.toggable');
            target.trigger('close.cs.toggable');
            target.slideUp(config_1.default.ANIMATION_TIMING, function () {
                target.trigger('closed.cs.toggable');
            });
        }
    }
}
exports.default = Toggables;


/***/ }),

/***/ "./assets/ts/modules/togglers.ts":
/*!***************************************!*\
  !*** ./assets/ts/modules/togglers.ts ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
class Togglers {
    constructor() { }
    init() {
        $(document).on('click.togglers', '[data-toggle]', function (e) {
            e.preventDefault();
            let elem = $(this);
            let toggle_class = elem.data('toggle');
            if (toggle_class.indexOf('.') >= 0) {
                toggle_class = toggle_class.replace(/\./g, ' ').trim();
            }
            let toggle_target;
            if (elem.attr('href')) {
                toggle_target = elem.attr('href');
            }
            else if (elem.data('target')) {
                toggle_target = elem.data('target');
            }
            else {
                toggle_target = elem;
            }
            $(toggle_target).toggleClass(toggle_class);
            $(toggle_target).trigger('toggle.cs.toggable');
        });
    }
}
exports.default = Togglers;


/***/ }),

/***/ 0:
/*!*************************************************************************************************************************************************!*\
  !*** multi ./assets/ts/codesmith-actions.ts ./assets/scss/codesmith-engine.scss ./assets/scss/codesmith-theme.scss ./assets/scss/tutorial.scss ***!
  \*************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! D:\Wamp\www\codesmith\assets\ts\codesmith-actions.ts */"./assets/ts/codesmith-actions.ts");
__webpack_require__(/*! D:\Wamp\www\codesmith\assets\scss\codesmith-engine.scss */"./assets/scss/codesmith-engine.scss");
__webpack_require__(/*! D:\Wamp\www\codesmith\assets\scss\codesmith-theme.scss */"./assets/scss/codesmith-theme.scss");
module.exports = __webpack_require__(/*! D:\Wamp\www\codesmith\assets\scss\tutorial.scss */"./assets/scss/tutorial.scss");


/***/ })

/******/ });
//# sourceMappingURL=codesmith-actions.js.map