$(function() {
	Codesmith.reveal.init({
		selector: '.reveal'
	});
	$('#modal-programmatic-setup').on('click', function(e) {
		e.preventDefault();
		var modal = Codesmith.modals.setup('#modal-setup');
		modal.open();
	});
	$('#modal-programmatic-create').on('click', function(e) {
		e.preventDefault();
		var modal = Codesmith.modals.create({
			header: '<h3>Programmatic modal</h3>',
			body: '<p>Body paragraph</p><img class="img-block" src="../assets/images/anvil.jpg" alt="" />',
			footer: '<p>Additional text.</p>',
			actions: [
				{
					action: 'close',
					text: 'Close modal',
					classes: 'btn-info',
				},
				{
					action: 'function',
					text: 'Alert me',
					classes: 'btn-warning',
					handler: function() {
						alert('Be careful!');
					}
				},
			]
		});
		modal.open();
	});
});
