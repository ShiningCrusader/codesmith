<?php include 'partials/head.php'; ?>
<?php include 'partials/header.php'; ?>
    <main class="cont">
        <h1>Alerts</h1>
		<div class="label-group">
			<span class="label label-engine">Engine</span>
		</div>
		<p>Alerts are an easy way to grasp the attention of your visitors but you'll need to give them a little color yourself if you're not using a theme.</p>
		<p>The basic syntax is the following:</p>
		<div class="clear-both"></div>
		<div class="alert">
			This is an alert
		</div>
		<pre><code class="language-html">&lt;div class="alert"&gt;This is an alert&lt;/div&gt;</code></pre>
		<p>Alerts work fine with simple text inside of them. If you need html tags or other content, you should wrap it in a <code>p</code> or <code>div</code>.</p>
		<p>There is not much to see without a theme beside a basic padding, so let's add one.</p>
    </main>
	<hr class="cont">
	<section class="cont">
		<h3>Colors</h3>
		<div class="label-group">
			<span class="label label-theme">Theme</span>
		</div>
		<p>Alerts come in 7 flavors:</p>
		<div class="clear-both"></div>
		<?php
			$flavors = ['primary', 'secondary', 'accent', 'neutral', 'info', 'success', 'error', 'warning'];
			foreach ($flavors as $_flavor):
		?>
			<div class="alert alert-<?php echo $_flavor; ?>">
				<p>
					This is an alert with <code class="language-css">.alert-<?php echo $_flavor; ?></code>
				</p>
			</div>
		<?php endforeach; ?>
		<p><code class="language-css">.alert-primary</code>, <code class="language-css">.alert-secondary</code> and <code class="language-css">.alert-accent</code> are dependant on this theme and their look may vary a lot when you configure a new theme.</p>
	</section>
	<hr class="cont">
	<section class="cont">
		<h3>Dismissable alerts</h3>
		<div class="label-group">
			<span class="label label-actions">Actions</span>
		</div>
		<p>You can create a dismissable alert adding a div with <code class="language-css">.alert-dimiss</code> anywhere inside the alert.</p>
		<div class="clear-both"></div>
		<div class="alert alert-success">
			<p>This is a dismissable alert</p>
			<div class="alert-dismiss">
				<i class="material-icons">close</i>
			</div>
		</div>
		<pre><code class="language-html">&lt;div class=&quot;alert alert-success&quot;&gt;&#13;&#10;&Tab;&lt;p&gt;This is a dismissable alert&lt;/p&gt;&#13;&#10;&Tab;&lt;div class=&quot;alert-dismiss&quot;&gt;&#13;&#10;&Tab;&Tab;&lt;i class=&quot;material-icons&quot;&gt;close&lt;/i&gt;&#13;&#10;&Tab;&lt;/div&gt;&#13;&#10;&lt;/div&gt;</code></pre>
	</section>
	<hr class="cont">
	<section class="cont">
		<h3>Icons</h3>
		<div class="label-group">
			<span class="label label-engine">Engine</span>
		</div>
		<p>You can easily add an <code>i</code> element and it will be centered vertically, keeping its distance from the content. Remember to wrap your content in a paragraph or div for it to display properly.</p>
		<div class="alert alert-info">
			<i class="material-icons">message</i>
			<p>This is an alert with an icon</p>
		</div>
		<pre><code class="language-html">&lt;div class=&quot;alert alert-info&quot;&gt;&#13;&#10;&Tab;&lt;i class=&quot;material-icons&quot;&gt;message&lt;/i&gt;&#13;&#10;&Tab;&lt;p&gt;This is an alert with an icon&lt;/p&gt;&#13;&#10;&lt;/div&gt;</code></pre>
	</section>
<?php include 'partials/footer.php'; ?>
