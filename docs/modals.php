<?php include 'partials/head.php'; ?>
<?php include 'partials/header.php'; ?>
<main class="cont">
	<h1>Modals</h1>
	<p>
		Modals have a simple markup. To open a modal, just add the <code class="language-css">[data-modal]</code> with a selector to your modal to any element and that element will trigger the modal when clicked. You can also specify the selector in an <code class="language-html">href</code> attribute, like in this example.
	</p>
	<p>
		<a class="btn btn-primary" href="#modal" data-modal>Open modal</a>
	</p>
	<div id="modal" class="modal fade modal">
		<div class="modal-header">
			<h3>Modal header</h3>
		</div>
		<div class="modal-body">
			<p>
				Modal body
			</p>
		</div>
		<div class="modal-footer">
			<div class="modal-actions">
				<a href="#modal" class="btn btn-default" data-modal-close>
					Close
				</a>
			</div>
		</div>
	</div>
	<p>Likewise, adding the <code class="language-css">[data-modal-close]</code> attribute to an element will close the selected modal.</p>
	<pre><code class="language-html">&lt;div id=&quot;modal&quot; class=&quot;modal fade modal&quot;&gt;&#13;&#10;&Tab;&lt;div class=&quot;modal-header&quot;&gt;&#13;&#10;&Tab;&Tab;&lt;h3&gt;Modal header&lt;/h3&gt;&#13;&#10;&Tab;&lt;/div&gt;&#13;&#10;&Tab;&lt;div class=&quot;modal-body&quot;&gt;&#13;&#10;&Tab;&Tab;&lt;p&gt;&#13;&#10;&Tab;&Tab;&Tab;Modal body&#13;&#10;&Tab;&Tab;&lt;/p&gt;&#13;&#10;&Tab;&lt;/div&gt;&#13;&#10;&Tab;&lt;div class=&quot;modal-footer&quot;&gt;&#13;&#10;&Tab;&Tab;&lt;div class=&quot;modal-actions&quot;&gt;&#13;&#10;&Tab;&Tab;&Tab;&lt;a href=&quot;#modal&quot; class=&quot;btn btn-default&quot; data-modal-close&gt;&#13;&#10;&Tab;&Tab;&Tab;&Tab;Close&#13;&#10;&Tab;&Tab;&Tab;&lt;/a&gt;&#13;&#10;&Tab;&Tab;&lt;/div&gt;&#13;&#10;&Tab;&lt;/div&gt;&#13;&#10;&lt;/div&gt;</code></pre>
</main>
<hr class="cont">
<section class="cont">
	<h3>Customizing the modal</h3>
	<p>If you don't want the fade-in animation just remove the <code class="language-css">.fade</code> class.</p>
	<p>
		The modal can occupy more or less screen space by using the <code class="language-css">.modal-big</code> and <code class="language-css">.modal-small</code> classes.
	</p>
	<p>
		You can position the modal inside the window using <code class="language-css">.top</code>, <code class="language-css">.bottom</code>, <code class="language-css">.left</code> and <code class="language-css">.right</code>
	</p>
	<p>You can also add flavors to the modal that will change the color of the header.</p>
	<p>
		<?php
		$flavors = ['primary', 'secondary', 'accent', 'neutral', 'info', 'success', 'error', 'warning'];
		foreach ($flavors as $_flavor):
			?>
			<a class="btn btn-<?php echo $_flavor; ?>" href="#modal-<?php echo $_flavor; ?>" data-modal>Open <?php echo $_flavor; ?> modal</a>
		<?php endforeach; ?>
	</p>
	<?php
	$flavors = ['primary', 'secondary', 'accent', 'neutral', 'info', 'success', 'error', 'warning'];
	foreach ($flavors as $_flavor):
		?>
		<div id="modal-<?php echo $_flavor; ?>" class="modal fade modal-<?php echo $_flavor; ?>">
			<div class="modal-header">
				<h3>Modal <?php echo $_flavor; ?></h3>
			</div>
			<div class="modal-body">
				<p>
					Modal body
				</p>
			</div>
			<div class="modal-footer">
				<div class="modal-actions">
					<a href="#modal-<?php echo $_flavor; ?>" class="btn btn-default" data-modal-close>
						Close
					</a>
				</div>
			</div>
		</div>
	<?php endforeach; ?>
	<p><code class="language-css">.modal-primary</code>, <code class="language-css">.modal-secondary</code> and <code class="language-css">.modal-accent</code> are dependant on this theme and their look may vary a lot when you configure a new theme.</p>
</section>
<hr class="cont">
<section class="cont">
	<h3>Using modals programmatically</h3>
	<p>Invoking the <code class="language-js">Codesmith.modals.setup()</code> method allows you to open an existing modal through javascript. The method requires a jQuery selector for the modal and returns the <code class="language-js">Modal</code> object. You can then invoke methods <code class="language-js">open()</code> and <code class="language-js">close()</code> at will.</p>
	<p><a id="modal-programmatic-setup" href="" class="btn">Setup modal</a></p>
	<pre><code class="language-js">$(&apos;#modal-programmatic-setup&apos;).on(&apos;click&apos;, function(e) {&#13;&#10;&Tab;e.preventDefault();&#13;&#10;&Tab;var modal = Codesmith.modals.setup(&apos;#modal-setup&apos;);&#13;&#10;&Tab;modal.open();&#13;&#10;});</code></pre>
	<div id="modal-setup" class="modal fade modal">
		<div class="modal-header">
			<h3>Modal header</h3>
		</div>
		<div class="modal-body">
			<p>
				Modal body
			</p>
		</div>
		<div class="modal-footer">
			<a href="#modal" class="btn btn-default" data-modal-close>
				Close
			</a>
		</div>
	</div>
	<p>You can also create a modal entirely from javascript using the <code class="language-js">Codesmith.modals.create()</code> method. The method requires a javascript object with various options.</p>
	<p><a id="modal-programmatic-create" href="" class="btn">Create modal</a></p>
	<pre><code class="language-js">$(&apos;#modal-programmatic-create&apos;).on(&apos;click&apos;, function(e) {&#13;&#10;&Tab;e.preventDefault();&#13;&#10;&Tab;var modal = Codesmith.modals.create({&#13;&#10;&Tab;&Tab;header: &apos;&lt;h3&gt;Programmatic modal&lt;/h3&gt;&apos;,&#13;&#10;&Tab;&Tab;body: &apos;&lt;p&gt;Body paragraph&lt;/p&gt;&lt;img class=&quot;img-block&quot; src=&quot;../assets/images/anvil.jpg&quot; alt=&quot;&quot; /&gt;&apos;,&#13;&#10;&Tab;&Tab;footer: &apos;&lt;p&gt;Additional text.&lt;/p&gt;&apos;,&#13;&#10;&Tab;&Tab;actions: [&#13;&#10;&Tab;&Tab;&Tab;{&#13;&#10;&Tab;&Tab;&Tab;&Tab;action: &apos;close&apos;,&#13;&#10;&Tab;&Tab;&Tab;&Tab;text: &apos;Close modal&apos;,&#13;&#10;&Tab;&Tab;&Tab;&Tab;classes: &apos;btn-info&apos;,&#13;&#10;&Tab;&Tab;&Tab;},&#13;&#10;&Tab;&Tab;&Tab;{&#13;&#10;&Tab;&Tab;&Tab;&Tab;action: &apos;function&apos;,&#13;&#10;&Tab;&Tab;&Tab;&Tab;text: &apos;Alert me&apos;,&#13;&#10;&Tab;&Tab;&Tab;&Tab;classes: &apos;btn-warning&apos;,&#13;&#10;&Tab;&Tab;&Tab;&Tab;handler: function() {&#13;&#10;&Tab;&Tab;&Tab;&Tab;&Tab;alert(&apos;Be careful!&apos;);&#13;&#10;&Tab;&Tab;&Tab;&Tab;}&#13;&#10;&Tab;&Tab;&Tab;},&#13;&#10;&Tab;&Tab;]&#13;&#10;&Tab;});&#13;&#10;&Tab;modal.open();&#13;&#10;});</code></pre>
	<h3>Modal options</h3>
	<div class="table-responsive">
		<table class="table bordered">
			<thead>
				<tr>
					<th>Option</th>
					<th>Type</th>
					<th>Description</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>header</td>
					<td>String</td>
					<td>Text for the modal header. Accepts html.</td>
				</tr>
				<tr>
					<td>body</td>
					<td>String</td>
					<td>Text for the modal body. Accepts html.</td>
				</tr>
				<tr>
					<td>footer</td>
					<td>String</td>
					<td>Text for the modal footer. Accepts html.</td>
				</tr>
				<tr>
					<td>fade</td>
					<td>Boolean</td>
					<td>Adds fade animations to the modal. Defaults to <code class="language-js">True</code>.</td>
				</tr>
				<tr>
					<td>actions</td>
					<td>Array</td>
					<td>Creates a <code class="language-js">ModalButton</code> object for every entry, according to its options. The button will execute the selected action when clicked. Refer to the documentation below for options.</td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="alert alert-warning">
		<i class="material-icons">warning</i>
		<p>Header, body and footer options are not strictly required, but you should provide at least one of them to make the modal user-friendly.</p>
	</div>
	<h3>Modal Button options</h3>
	<div class="table-responsive">
		<table class="table bordered">
			<thead>
				<tr>
					<th>Option</th>
					<th>Type</th>
					<th>Description</th>
					<th>Required</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>action</td>
					<td>String</td>
					<td>
						Accepted values:
						<ul>
							<li><code class="language-js">'close'</code>: closes the modal</li>
							<li><code class="language-js">'function'</code>: executes the specified handler</li>
						</ul>
					</td>
					<td>Yes</td>
				</tr>
				<tr>
					<td>text</td>
					<td>String</td>
					<td>Text for the button. Accepts html.</td>
					<td>Yes</td>
				</tr>
				<tr>
					<td>classes</td>
					<td>String</td>
					<td>Additional classes to style the button. <code class="language-css">.btn</code> is added by default.</td>
					<td></td>
				</tr>
				<tr>
					<td>handler</td>
					<td>Function</td>
					<td>Function to be called when clicking on the button.</td>
					<td>Yes, if action is <code class="language-js">'function'</code>.</td>
				</tr>
			</tbody>
		</table>
	</div>
</section>
<?php include 'partials/footer.php'; ?>
