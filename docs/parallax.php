<?php include 'partials/head.php'; ?>
<?php include 'partials/header.php'; ?>
        <main class="cont">
            <h1>Parallax</h1>
			<div class="label-group">
				<span class="label label-plugins">Plugins</span>
			</div>
			<p>This effect is really easy to implement but only available for backgrounds. If you need complex parallax effects, consider using an external plugin.</p>
        </main>
		<hr class="cont">
		<section class="cont">
			<h3>Requirements</h3>
			<p>For the effect to work, you need an element that has a background image that covers the entire area. <code class="language-css">.bg-cover</code> does the job for you. The background image must also be proportionally taller than the element it's applied to. If the image is proportionally too short, it will cover the element, but won't animate when scrolling the page.</p>
			<p>Lastly, the background must not be fixed.</p>
		</section>
		<hr class="cont">
		<section class="cont">
			<h3>Usage</h3>
			<p>To enable the effect you only need to add a data attribute <code class="language-css">[data-parallax=""]</code> to the element. The value of the data attribute determines the scrolling speed.</p>
			<div class="table-responsive">
				<table class="table bordered">
					<thead>
						<tr>
							<th>Value</th>
							<th>Effect</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>0</td>
							<td>The background does not scroll.</td>
						</tr>
						<tr>
							<td>Between 0 and 1</td>
							<td>The background scrolls faster as the value increases.</td>
						</tr>
						<tr>
							<td>1</td>
							<td>The background scrolls as fast as possible without showing gaps when the element is at the edge of the window.</td>
						</tr>
						<tr>
							<td>Greater than 2</td>
							<td>Not recommended. The background scrolls much faster, potentially showing gaps when the element is at the edge of the page. Use this only if you have a really tall image.</td>
						</tr>
					</tbody>
				</table>
			</div>
			<p>I'll save you the calculations, but utlimately it's the height of the image that determines the base scrolling speed. The <code class="language-css">[data-parallax]</code> attribute only multiplies that speed. You'll have to fine-tune the speed depending on your image's proportion to make it look smooth while scrolling.</p>
			<div class="row">
				<div class="col-s-12 col-m-6">
					<h5>Parallax speed: 0</h5>
					<div class="bg-cover pf6e" style="background-image: url(../assets/images/crucible.jpg);" data-parallax="0">
					</div>
				</div>
				<div class="col-s-12 col-m-6">
					<h5>Parallax speed: 0.5</h5>
					<div class="bg-cover pf6e" style="background-image: url(../assets/images/crucible.jpg);" data-parallax="0.5">
					</div>
				</div>
				<div class="col-s-12 col-m-6">
					<h5>Parallax speed: 1</h5>
					<div class="bg-cover pf6e" style="background-image: url(../assets/images/crucible.jpg);" data-parallax="1">
					</div>
				</div>
				<div class="col-s-12 col-m-6">
					<h5>Parallax speed: 2</h5>
					<div class="bg-cover pf6e" style="background-image: url(../assets/images/crucible.jpg);" data-parallax="2">
					</div>
				</div>
			</div>
			<div style="height: 50vh;">
				<p>This space is needed to try out the parallax effect in this environment.</p>
			</div>
		</section>
<?php include 'partials/footer.php'; ?>
