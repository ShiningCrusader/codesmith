<?php include 'partials/head.php'; ?>
<?php include 'partials/header.php'; ?>
        <main class="cont">
			<h1>Engine helpers</h1>
			<div class="label-group">
				<span class="label label-engine">Engine</span>
			</div>
			<p>
				Helpers are simple classes, often used to apply a single property to your elements.
			</p>
        </main>
		<hr class="cont">
		<section class="cont">
			<h3>Display</h3>
			<p>
				<code class="language-css">.block</code>, <code class="language-css">.inline-block</code>, <code class="language-css">.inline</code>, <code class="language-css">.flex</code> and <code class="language-css">.none</code> will set the display property.
			</p>
			<p>
				There are also responsive versions, like <code class="language-css">.block-m</code>, <code class="language-css">.block-l</code> and <code class="language-css">.block-h</code>, triggering that display property starting at the specified breakpoint.
			</p>
		</section>
		<hr class="cont">
		<section class="cont">
			<h3>Widths</h3>
			<p>
				Width classes like <code class="language-css">.width-0</code>, <code class="language-css">.width-15</code> and <code class="language-css">.width-65</code> range from 0% to 100% at steps of 5% each and are most useful when designing tables.
			</p>
		</section>
		<hr class="cont">
		<section class="cont">
			<h3>Float</h3>
			<p>
				There are <code class="language-css">.float-left</code>, <code class="language-css">.float-right</code> and <code class="language-css">.float-none</code> for floating and <code class="language-css">.clear-left</code>, <code class="language-css">.clear-right</code> and <code class="language-css">.clear-both</code> for clearing floats.
			</p>
		</section>
		<hr class="cont">
		<section class="cont">
			<h3>Text</h3>
			<p>
				<code class="language-css">.txt-big</code> and <code class="language-css">.txt-small</code> will help you set font size at 0.9em and 1.2em.
			</p>
			<p>
				Alignment can be set with <code class="language-css">.txt-left</code>, <code class="language-css">.txt-right</code>, <code class="language-css">.txt-center</code> and <code class="language-css">.txt-justify</code>.
			</p>
		</section>
		<hr class="cont">
		<section class="cont">
			<h3>Flex</h3>
			<p>
				These classes will help you to deal quickly with any Flex or Grid element within in the framework.
			</p>
			<?php
				$classes = array(
					".flex-row" => "display: flex;<br>flex-direction: row;",
					".flex-row-reverse" => "display: flex;<br>flex-direction: row-reverse;",
					".flex-col" => "display: flex;<br>flex-direction: column;",
					".flex-col-reverse" => "display: flex;<br>flex-direction: column-reverse;",
					".flex-wrap" => "flex-wrap: wrap;",
					".justify-content-start" => "justify-content: flex-start;",
					".justify-content-center" => "justify-content: center;",
					".justify-content-end" => "justify-content: flex-end;",
					".justify-content-between" => "justify-content: space-between;",
					".justify-content-around" => "justify-content: space-around;",
					".align-items-start" => "align-items: flex-start;",
					".align-items-center" => "align-items: center;",
					".align-items-end" => "align-items: flex-end;",
					".align-items-stretch" => "align-items: stretch;",
					".align-self-start" => "align-self: flex-start;",
					".align-self-center" => "align-self: center;",
					".align-self-end" => "align-self: flex-end;",
					".align-self-stretch" => "align-self: stretch;"
				);
			?>
			<table class="table bordered tinted">
				<thead>
					<tr>
						<th>Class</th>
						<th>Properties</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($classes as $_class => $_properties): ?>
						<tr>
							<td><code class="language-css"><?php echo $_class; ?></code></td>
							<td><?php echo $_properties; ?></td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</section>
		<hr class="cont">
		<section class="cont">
			<h3>Margins and paddings</h3>
			<p>
				It's a lot harder to explain them than it is to use them.<br>
				They are a series of classes to set paddings and margins really quickly, with different units.<br>
			</p>
			<p>
				Take this class, as an example: <code class="language-css">.mb5p</code><br>
				Now, let's split its characters and see what each one means.
			</p>
			<table class="table compact bordered">
				<thead>
					<tr>
						<th class="txt-center">.</th>
						<th class="txt-center">m</th>
						<th class="txt-center">b</th>
						<th class="txt-center">5</th>
						<th class="txt-center">p</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="txt-center">Class selector</td>
						<td class="txt-center">Margin</td>
						<td class="txt-center">Bottom</td>
						<td class="txt-center">5</td>
						<td class="txt-center">Pixels</td>
					</tr>
				</tbody>
			</table>
			<p>
				Following this format, you can set all classes. Here's a quick reference table minus the class selector:
			</p>
			<div class="table-responsive">
				<table class="table bordered tinted">
					<thead>
						<tr>
							<th>Property</th>
							<th>Direction</th>
							<th>Size</th>
							<th>Units</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>
								m - margin<br>
								p - padding
							</td>
							<td>
								t - top<br>
								r - right<br>
								b - bottom<br>
								l - left<br>
								v - vertical (top and bottom)<br>
								h - horizontal (right and left)<br>
								f - full (all directions)
							</td>
							<td>
								from 0 to 10 for em<br>
								from 0 to 50 with steps of 5 for pixels
							</td>
							<td>
								e - em<br>
								p - pixels
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<p>
				One last thing: you can set auto margins with classes like <code class="language-css">.mba</code> and <code class="language-css">.mha</code>. Of course, you don't have to specify units in this case.
			</p>
		</section>
		<hr class="cont">
		<section class="cont">
			<h3>Backgrounds</h3>
			<p>
				These classes can help you positioning backgrounds. Just remember to set the  background-image property in your css or inline.
			</p>
			<div class="table-responsive">
				<table class="table bordered tinted">
					<thead>
						<tr>
							<th>Class</th>
							<th>Effect</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><code class="language-css">.bg-contain</code></td>
							<td>The background fits the container at the largest possible size, keeping its aspect ratio.</td>
						</tr>
						<tr>
							<td><code class="language-css">.bg-cover</code></td>
							<td>The background covers the whole element, keeping its aspect ratio.</td>
						</tr>
						<tr>
							<td><code class="language-css">.bg-fixed</code></td>
							<td>Behaves like <code class="language-css">.bg-cover</code> but keeps the background image fixed making it look like the content slides over it, instead of moving with it.</td>
						</tr>
						<tr>
							<td><code class="language-css">.bg-pattern</code></td>
							<td>The background keeps its original size, is centered and tiled over the surface of the element.</td>
						</tr>
					</tbody>
				</table>
			</div>
		</section>
		<hr class="cont">
		<section class="cont">
			<h3>Special helpers</h3>
			<h4>Images</h4>
			<p>
				<code class="language-css">.img-block</code> sets the image at the best possible width, keeping its size and displaying centered when there's more room.<br>
				Resize the window and see for yourself.
			</p>
			<img class="img-block" src="../assets/images/anvil-small.jpg">
			<h4>Block elements</h4>
			<p>
				<code class="language-css">.full-window</code> is a simple class that makes the element span the full width and height of the window. Useful for captivating landing pages.
			</p>
			<p>
				<code class="language-css">.filler</code> is a class that sounds very situational, but I've found myself writing way too many times in my projects.<br>
				Basically, it sets the element to full height and displays its content in a flex column. It's very useful when you have rows of products or similar items that have an image, their name and a short description with a "view more" link.
				This class will help you keep similar elements at the same height instead of showing up as a jagged staircase when the content is different for each item.
			</p>
		</section>
<?php include 'partials/footer.php'; ?>
