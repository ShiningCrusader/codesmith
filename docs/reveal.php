<?php include 'partials/head.php'; ?>
<?php include 'partials/header.php'; ?>
        <main class="cont">
            <h1>Reveal</h1>
			<div class="label-group">
				<span class="label label-plugins">Plugins</span>
			</div>
			<p>This javascript action will trigger animation for selected elements when they come into view.</p>
        </main>
		<hr class="cont">
		<section class="cont">
			<h3>Usage</h3>
			<p>Add a class or ID to the elements you want to animate and initialize the plugin. The elements should have a css animation defined beforehand. The plugin only prevents the animation from playing until the element is actually visible.</p>
			<pre><code class="language-javascript">Codesmith.reveal.init({&#13;&#10;&Tab;selector: &apos;.reveal&apos;&#13;&#10;});</code></pre>
			<div id="first" class="border-2 border-primary pf2e reveal reveal-tutorial">
				<h5>First block</h5>
			</div>
			<div id="second" class="border-2 border-secondary pf2e reveal reveal-tutorial">
				<h5>Second block</h5>
			</div>
			<div id="third" class="border-2 border-accent pf2e reveal reveal-tutorial">
				<h5>Third block</h5>
			</div>
			<div id="fourth" class="border-2 border-neutral pf2e reveal reveal-tutorial">
				<h5>Fourth block</h5>
			</div>
			<div id="fifth" class="border-2 border-info pf2e reveal reveal-tutorial">
				<h5>Fifth block</h5>
			</div>
		</section>
<?php include 'partials/footer.php'; ?>
