<?php include 'partials/head.php'; ?>
<?php include 'partials/header.php'; ?>
        <main id="home-main" class="bg-cover" data-parallax="1">
			<div class="cont">
				<div class="row justify-content-center align-items-center">
					<div class="col-s-4 col-m-3 order-m-2 col-l-3 col-h-2 mha">
						<img class="img-block" src="../assets/images/logo-vertical.png" alt="Codesmith">
					</div>
					<div class="col-s-10 col-m-4 order-m-1 mha">
						<h1 class="txt-center">
							The new CSS framework
						</h1>
						<div class="row">
							<div class="col-s-6">
								<a href="about.php" class="btn btn-primary width-100">About</a>
							</div>
							<div class="col-s-6">
								<a href="https://gitlab.com/ShiningCrusader/codesmith" class="btn btn-secondary width-100">Download</a>
							</div>
						</div>
					</div>
				</div>
			</div>
        </main>
		<section class="cont">
			<div class="row justify-content-center">
				<div class="col-s-12 col-m-10 col-l-8">
					<p class="txt-center txt-big">Codesmith is a mobile-first framework, inspired by Bootstrap and heavily based on flexbox, that aims to minimize the code you need to write and keep customization easy.</p>
				</div>
			</div>
		</section>
		<hr class="cont">
		<section id="home-features" class="cont">
			<div class="row-exp justify-content-around">
				<div class="col-s-12 col-m-4 col-l-3">
					<i class="material-icons bg-secondary border-round">devices</i>
					<h3>Responsive</h3>
					<p>Codesmith looks good on any modern device, regardless of screen width or zoom.</p>
				</div>
				<div class="col-s-12 col-m-4 col-l-3">
					<i class="material-icons bg-secondary border-round">keyboard</i>
					<h3>Developer friendly</h3>
					<p>Less code to write and less clutter means faster production times.</p>
				</div>
				<div class="col-s-12 col-m-4 col-l-3">
					<i class="material-icons bg-secondary border-round">brush</i>
					<h3>Customizable</h3>
					<p>A lightweight theme is all you need to start building little details.</p>
				</div>
			</div>
		</section>
		<section id="home-buttons" data-parallax="1">
			<div class="cont">
				<div class="row justify-content-center">
					<div class="col-s-12 tagline">
						What are you waiting for?<br class="inline-s">Get smithin'!
					</div>
					<div class="col-s-12 col-m-4">
						<a href="https://gitlab.com/ShiningCrusader/codesmith" class="btn btn-secondary block" target="_blank">
							<i class="material-icons">code</i>
							View on GitLab
						</a>
					</div>
				</div>
			</div>
		</section>
<?php include 'partials/footer.php'; ?>
