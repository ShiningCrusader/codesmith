<?php include 'partials/head.php'; ?>
<?php include 'partials/header.php'; ?>
        <main class="cont">
			<h1>Other elements</h1>
			<div class="label-group">
				<span class="label label-theme">Theme</span>
			</div>
			<p>
				These elements only need the theme file to work since their markup is nothing special.
			</p>
        </main>
		<hr class="cont">
		<section class="cont">
			<h3>Breadcrumb</h3>
			<ul class="breadcrumb">
				<li>
					<a href="">
						<i class="material-icons">home</i>
					</a>
				</li>
				<li><a href="">link</a></li>
				<li><a href="">link</a></li>
				<li>link</li>
			</ul>
			<pre><code class="language-html">&lt;ul class=&quot;breadcrumb&quot;&gt;&#13;&#10;&Tab;&lt;li&gt;&#13;&#10;&Tab;&Tab;&lt;a href=&quot;&quot;&gt;&#13;&#10;&Tab;&Tab;&Tab;&lt;i class=&quot;material-icons&quot;&gt;home&lt;/i&gt;&#13;&#10;&Tab;&Tab;&lt;/a&gt;&#13;&#10;&Tab;&lt;/li&gt;&#13;&#10;&Tab;&lt;li&gt;&lt;a href=&quot;&quot;&gt;link&lt;/a&gt;&lt;/li&gt;&#13;&#10;&Tab;&lt;li&gt;&lt;a href=&quot;&quot;&gt;link&lt;/a&gt;&lt;/li&gt;&#13;&#10;&Tab;&lt;li&gt;link&lt;/li&gt;&#13;&#10;&lt;/ul&gt;</code></pre>
		</section>
		<hr class="cont">
		<section class="cont">
			<h3>Pagination</h3>
			<ul class="pagination">
				<li>
					<a href="">
						<i class="material-icons">first_page</i>
					</a>
				</li>
				<li><a href="">1</a></li>
				<li><a href="">2</a></li>
				<li>...</li>
				<li>
					<a href="">
						<i class="material-icons">last_page</i>
					</a>
				</li>
			</ul>
			<pre><code class="language-html">&lt;ul class=&quot;pagination&quot;&gt;&#13;&#10;&Tab;&lt;li&gt;&#13;&#10;&Tab;&Tab;&lt;a href=&quot;&quot;&gt;&#13;&#10;&Tab;&Tab;&Tab;&lt;i class=&quot;material-icons&quot;&gt;first_page&lt;/i&gt;&#13;&#10;&Tab;&Tab;&lt;/a&gt;&#13;&#10;&Tab;&lt;/li&gt;&#13;&#10;&Tab;&lt;li&gt;&lt;a href=&quot;&quot;&gt;1&lt;/a&gt;&lt;/li&gt;&#13;&#10;&Tab;&lt;li&gt;&lt;a href=&quot;&quot;&gt;2&lt;/a&gt;&lt;/li&gt;&#13;&#10;&Tab;&lt;li&gt;...&lt;/li&gt;&#13;&#10;&Tab;&lt;li&gt;&#13;&#10;&Tab;&Tab;&lt;a href=&quot;&quot;&gt;&#13;&#10;&Tab;&Tab;&Tab;&lt;i class=&quot;material-icons&quot;&gt;last_page&lt;/i&gt;&#13;&#10;&Tab;&Tab;&lt;/a&gt;&#13;&#10;&Tab;&lt;/li&gt;&#13;&#10;&lt;/ul&gt;</code></pre>
		</section>
		<hr class="cont">
		<section class="cont">
			<h3>Labels</h3>
			<span class="label">
				.label
			</span>
			<?php
				foreach (array('primary', 'secondary', 'accent', 'success', 'error', 'warning', 'info', 'neutral') as $_flavor):
			?>
				<span class="label label-<?php echo $_flavor; ?>">
					.label.label-<?php echo $_flavor; ?>
				</span>
			<?php endforeach; ?>
		</section>
		<hr class="cont">
		<section class="cont">
			<h3>Responsive video embed</h3>
			<p>To display embedded videos correctly at all sizes, wrap their <code>iframe</code> or <code>video</code> element in a div with the <code class="language-css">.video</code> class. This will cover all 16:9 videos. If your video has a different aspect ratio, add class <code class="language-css">.video-4by3</code> or <code class="language-css">.video-21by9</code>.</p>
			<div class="row">
				<div class="col-s-12 col-m-4">
					<p><code class="language-css">.video</code></p>
					<div class="video">
						<iframe src="https://www.youtube-nocookie.com/embed/fBGSJ3sbivI"></iframe>
					</div>
				</div>
				<div class="col-s-12 col-m-4">
					<p><code class="language-css">.video.video-4by3</code></p>
					<div class="video video-4by3">
						<iframe src="https://www.youtube-nocookie.com/embed/djV11Xbc914"></iframe>
					</div>
				</div>
				<div class="col-s-12 col-m-4">
					<p><code class="language-css">.video.video-21by9</code></p>
					<div class="video video-21by9">
						<iframe src="https://www.youtube-nocookie.com/embed/PaNYOzwlVC8"></iframe>
					</div>
				</div>
			</div>
		</section>
<?php include 'partials/footer.php'; ?>
