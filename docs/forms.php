<?php include 'partials/head.php'; ?>
<?php include 'partials/header.php'; ?>
        <main class="cont">
            <h1>Forms</h1>
			<div class="label-group">
				<span class="label label-theme">Theme</span>
				<span class="label label-actions">Actions</span>
			</div>
			<p>Form elements are a wild bunch because their looks differ a lot between browsers.</p>
			<p>In this case, the engine only does some minor positioning while the theme does the heavy work of uniforming inputs across browsers where possible.</p>
			<p>Actions are needed to handle checkboxes, radio buttons and file inputs.</p>
        </main>
		<hr class="cont">
		<section class="cont">
			<form class="form" action="">
				<label for="">Input label</label>
				<input class="form-input" type="text" name="text" placeholder="text input">
				<input class="form-input" type="password" name="password" placeholder="password input">
				<input class="form-input" type="number" name="number" placeholder="0">
				<input class="form-input" type="date" name="date">
				<select class="form-input"  name="select">
					<option value="" disabled>Select</option>
					<option value="">Option 1</option>
					<option value="">Option 2</option>
					<option value="">Option 3</option>
				</select>
				<div class="row">
					<div class="col-s-12 col-m-4">
						<div class="form-checkbox">
							<input id="checkbox1" class="form-input" type="checkbox" name="checkbox1">
							<label for="checkbox1">Checkbox 1</label>
						</div>
					</div>
					<div class="col-s-12 col-m-4">
						<div class="form-checkbox">
							<input id="checkbox2" class="form-input" type="checkbox" name="checkbox2">
							<label for="checkbox2">Checkbox 2</label>
						</div>
					</div>
					<div class="col-s-12 col-m-4">
						<div class="form-checkbox">
							<input id="checkbox3" class="form-input" type="checkbox" name="checkbox3">
							<label for="checkbox3">Checkbox 3</label>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-s-12 col-m-4">
						<div class="form-radio">
							<input id="radio1" class="form-input" type="radio" name="radio">
							<label for="radio1">Radio 1</label>
						</div>
					</div>
					<div class="col-s-12 col-m-4">
						<div class="form-radio">
							<input id="radio2" class="form-input" type="radio" name="radio">
							<label for="radio2">Radio 2</label>
						</div>
					</div>
					<div class="col-s-12 col-m-4">
						<div class="form-radio">
							<input id="radio3" class="form-input" type="radio" name="radio">
							<label for="radio3">Radio 3</label>
						</div>
					</div>
				</div>
				<textarea class="form-input" name="name" rows="3" placeholder="Text area"></textarea>
				<div class="form-file">
					<input id="file" type="file" name="file" data-selected-caption="{count} files selected" multiple>
					<label for="file">Drag files here or click to choose a file</label>
				</div>
				<button  class="btn" >Invia</button>
			</form>
		</section>
<?php include 'partials/footer.php'; ?>
