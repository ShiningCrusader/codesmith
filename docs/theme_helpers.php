<?php include 'partials/head.php'; ?>
<?php include 'partials/header.php'; ?>
        <main class="cont">
			<h1>Theme helpers</h1>
			<div class="label-group">
				<span class="label label-theme">Theme</span>
			</div>
			<p>
				Like the engine helpers, this is a collection of simple classes related to the theme.
			</p>
        </main>
		<hr class="cont">
		<section class="cont">
			<h3>Color variations</h3>
			<div class="table-responsive">
				<table class="table bordered">
					<tbody>
						<tr>
							<th>Color</th>
							<th>Light</th>
							<th>Normal</th>
							<th>Dark</th>
						</tr>
						<?php
							foreach (array('primary', 'secondary', 'accent', 'success', 'error', 'warning', 'info', 'neutral') as $_flavor):
						?>
							<tr>
								<td><?php echo ucfirst($_flavor); ?></td>
								<td>
									<div class="border-2 pf5p mb5p color-<?php echo $_flavor; ?>-light">.color-<?php echo $_flavor; ?>-light</div>
									<div class="border-2 pf5p mb5p bg-<?php echo $_flavor; ?>-light">.bg-<?php echo $_flavor; ?>-light</div>
									<div class="border-2 pf5p mb5p border-<?php echo $_flavor; ?>-light">.border-<?php echo $_flavor; ?>-light</div>
								</td>
								<td>
									<div class="border-2 pf5p mb5p color-<?php echo $_flavor; ?>">.color-<?php echo $_flavor; ?></div>
									<div class="border-2 pf5p mb5p bg-<?php echo $_flavor; ?>">.bg-<?php echo $_flavor; ?></div>
									<div class="border-2 pf5p mb5p border-<?php echo $_flavor; ?>">.border-<?php echo $_flavor; ?></div>
								</td>
								<td>
									<div class="border-2 pf5p mb5p color-<?php echo $_flavor; ?>-dark">.color-<?php echo $_flavor; ?>-dark</div>
									<div class="border-2 pf5p mb5p bg-<?php echo $_flavor; ?>-dark">.bg-<?php echo $_flavor; ?>-dark</div>
									<div class="border-2 pf5p mb5p border-<?php echo $_flavor; ?>-dark">.border-<?php echo $_flavor; ?>-dark</div>
								</td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			</div>
		</section>
		<hr class="cont">
		<section class="cont">
			<h3>Borders</h3>
			<p>
				Classes from <code class="language-css">.border-1</code> to <code class="language-css">.border-5</code> can set a transparent border in pixels.
			</p>
			<p>
				<span class="border-2 border-secondary pv5p ph10p">.border-2.border-secondary</span>
			</p>
			<p>
				The class <code class="language-css">.border-round</code> will set all borders rounded for elements up to 2048pixels wide or tall.
			</p>
			<p>
				<span class="border-round bg-secondary pv5p ph10p">.border-round.bg-secondary</span>
			</p>
		</section>
		<hr class="cont">
		<section class="cont">
			<h3>Shadow helpers</h3>
			<p>
				You can use classes from <code class="language-css">.shadow-1</code> to <code class="language-css">.shadow-20</code> to quickly add a shadow effect to a box.<br>
				If you need to animate them or generate higher shadows, the <code class="language-scss">@mixin shadow()</code> for scss is available.
			</p>
			<div class="row">
				<div class="col-s-12 col-m-4">
					<div class="card shadow-4"><div class="card-content">shadow-4</div></div>
				</div>
				<div class="col-s-12 col-m-4">
					<div class="card shadow-8"><div class="card-content">shadow-8</div></div>
				</div>
				<div class="col-s-12 col-m-4">
					<div class="card shadow-16"><div class="card-content">shadow-16</div></div>
				</div>
			</div>
		</section>
		<hr class="cont">
		<section class="cont">
			<h3>Special helpers</h3>
			<h4>Transitioned</h4>
			<p>The <code class="language-css">.transitioned</code> class applies a transition to all properties changing for the selected element. Really handy when working with <a href="toggables.php">toggables</a>.</p>
			<div class="mb1e">
				<a class="btn" href="#textresize" data-toggle="txt-big">Enlarge text</a>
				<div id="textresize" class="transitioned mt5p">Codesmith is awesome</div>
			</div>
		</section>
<?php include 'partials/footer.php'; ?>
