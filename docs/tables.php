<?php include 'partials/head.php'; ?>
<?php include 'partials/header.php'; ?>
    <main class="cont">
        <h1>Tables</h1>
		<div class="label-group">
			<span class="label label-engine">Engine</span>
		</div>
		<p>
			To use tables in Codesmith you need to add the <code class="language-css">.table</code> class to a <code>table</code> element. This will give you a full-width table, even if you have little content. To make the table use the least width possible, add the <code class="language-css">.compact</code> class.
		</p>
    </main>
	<hr class="cont">
	<section class="cont">
		<h3>Styling the table</h3>
		<div class="label-group">
			<span class="label label-theme">Theme</span>
		</div>
		<p>
			Tables don't have any other style on their own so you'll need a little styling and several classes are available just for that.</p>
		<div class="row-exp">
			<div class="col-s-12 col-m-6 col-l-4 flex-col">
				<h4>Tinted</h4>
				<p>The <code class="language-css">.tinted</code> class adds background color to the table <code>thead</code> element.</p>
				<table class="table tinted mta">
					<thead>
						<tr>
							<th colspan="2">.table.tinted</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>a</td>
							<td>b</td>
						</tr>
						<tr>
							<td>1</td>
							<td>2</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="col-s-12 col-m-6 col-l-4 flex-col">
				<h4>Striped</h4>
				<p>The <code class="language-css">.striped</code> class adds alternating background color to any <code>tbody &gt; tr</code> element.</p>
				<table class="table striped mta">
					<thead>
						<tr>
							<th colspan="2">.table.striped</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>a</td>
							<td>b</td>
						</tr>
						<tr>
							<td>1</td>
							<td>2</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="col-s-12 col-m-6 col-l-4 flex-col">
				<h4>Hovered</h4>
				<p>The <code class="language-css">.hovered</code> class adds an hover effect to every <code>tbody &gt; tr</code> element.</p>
				<table class="table hovered mta">
					<thead>
						<tr>
							<th colspan="2">.table.hovered</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>a</td>
							<td>b</td>
						</tr>
						<tr>
							<td>1</td>
							<td>2</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="col-s-12 col-m-6 col-l-4 flex-col">
				<h4>Lined</h4>
				<p>The <code class="language-css">.lined</code> class adds a thin line between <code>tr</code> elements.</p>
				<table class="table lined mta">
					<thead>
						<tr>
							<th colspan="2">.table.lined</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>a</td>
							<td>b</td>
						</tr>
						<tr>
							<td>1</td>
							<td>2</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="col-s-12 col-m-6 col-l-4 flex-col">
				<h4>Bordered</h4>
				<p>The <code class="language-css">.bordered</code> class a thin border to every <code>th</code> and <code>td</code> element.</p>
				<table class="table bordered mta">
					<thead>
						<tr>
							<th colspan="2">.table.bordered</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>a</td>
							<td>b</td>
						</tr>
						<tr>
							<td>1</td>
							<td>2</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="col-s-12 col-m-6 col-l-4 flex-col">
				<p>
					Of course, you can mix classes for multiple results.
				</p>
				<table class="table tinted striped bordered hovered mta">
					<thead>
						<tr>
							<th colspan="2">.table.tinted.striped.bordered.hovered</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>a</td>
							<td>b</td>
						</tr>
						<tr>
							<td>1</td>
							<td>2</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<h4>Padding</h4>
		<p>
			<code class="language-css">.table-con</code> and <code class="language-css">.table-exp</code> are also available and reduce or increase the cell padding respectively.
		</p>
		<div class="row-exp">
			<div class="col-s-12 col-m-4">
				<table class="table bordered">
					<thead>
						<tr>
							<th colspan="2">.table.bordered</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>a</td>
							<td>b</td>
						</tr>
						<tr>
							<td>1</td>
							<td>2</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="col-s-12 col-m-4">
				<table class="table-con bordered">
					<thead>
						<tr>
							<th colspan="2">.table-con.bordered</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>a</td>
							<td>b</td>
						</tr>
						<tr>
							<td>1</td>
							<td>2</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="col-s-12 col-m-4">
				<table class="table-exp bordered">
					<thead>
						<tr>
							<th colspan="2">.table-exp.bordered</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>a</td>
							<td>b</td>
						</tr>
						<tr>
							<td>1</td>
							<td>2</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<h4>Responsive</h4>
		<p>To make a table scroll horizontally on small devices, wrap it in an element with <code class="language-css">.table-responsive</code>.</p>
		<pre><code class="language-html">&lt;div class=&quot;table-responsive&quot;&gt;&#13;&#10;&Tab;&lt;table class=&quot;table tinted lined&quot;&gt;&#13;&#10;&Tab;&Tab;&lt;thead&gt;&#13;&#10;&Tab;&Tab;&Tab;&lt;tr&gt;&#13;&#10;&Tab;&Tab;&Tab;&Tab;&lt;th&gt;...&lt;/th&gt;&#13;&#10;&Tab;&Tab;&Tab;&lt;/tr&gt;&#13;&#10;&Tab;&Tab;&lt;/thead&gt;&#13;&#10;&Tab;&Tab;&lt;tbody&gt;&#13;&#10;&Tab;&Tab;&Tab;&lt;tr&gt;&#13;&#10;&Tab;&Tab;&Tab;&Tab;&lt;td&gt;...&lt;/td&gt;&#13;&#10;&Tab;&Tab;&Tab;&lt;/tr&gt;&#13;&#10;&Tab;&Tab;&lt;/tbody&gt;&#13;&#10;&Tab;&lt;/table&gt;&#13;&#10;&lt;/div&gt;</code></pre>
		<div class="table-responsive">
			<table class="table tinted lined">
				<thead>
					<tr>
						<th>Codesmith is awesome</th>
						<th>Codesmith is awesome</th>
						<th>Codesmith is awesome</th>
						<th>Codesmith is awesome</th>
						<th>Codesmith is awesome</th>
						<th>Codesmith is awesome</th>
						<th>Codesmith is awesome</th>
						<th>Codesmith is awesome</th>
						<th>Codesmith is awesome</th>
						<th>Codesmith is awesome</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>Codesmith is awesome</td>
						<td>Codesmith is awesome</td>
						<td>Codesmith is awesome</td>
						<td>Codesmith is awesome</td>
						<td>Codesmith is awesome</td>
						<td>Codesmith is awesome</td>
						<td>Codesmith is awesome</td>
						<td>Codesmith is awesome</td>
						<td>Codesmith is awesome</td>
						<td>Codesmith is awesome</td>
					</tr>
				</tbody>
			</table>
		</div>
	</section>
<?php include 'partials/footer.php'; ?>
