<?php include 'partials/head.php'; ?>
<?php include 'partials/header.php'; ?>
        <main class="cont">
			<h1>Cards</h1>
			<p>
				Cards are one of the most requested elements lately and codesmith offers a few simple classes to lay the foundation for further customization.
			</p>
        </main>
		<hr class="cont">
		<section class="cont">
			<h3>Basic cards</h3>
			<div class="label-group">
				<span class="label label-engine">Engine</span>
				<span class="label label-theme">Theme</span>
			</div>
			<p>Cards are flexboxes as well, and they require two classes for correct setup.</p>
			<p>The base markup is as follows:</p>
			<pre><code class="language-html">&lt;div class=&quot;card&quot;&gt;&#13;&#10;&Tab;&lt;div class=&quot;card-content&quot;&gt;&#13;&#10;&Tab;&Tab;...&#13;&#10;&Tab;&lt;/div&gt;&#13;&#10;&lt;/div&gt;</code></pre>
			<p>Basically, everything you need to write in the card goes inside the <code class="language-css">.card-content</code> element. If you want to split the card vertically, just add the <code class="language-css">.horizontal</code> class.</p>
			<p><code class="language-css">.horizontal-m</code>, <code class="language-css">.horizontal-l</code> and <code class="language-css">.horizontal-h</code> are also available to display a card horizontally only above a breakpoint.</p>
			<div class="row">
				<div class="col-s-12 col-m-6">
					<div class="card">
						<div class="card-content">
							<h3>Simple card</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
						</div>
					</div>
				</div>
			</div>
		</section>
		<hr class="cont">
		<section class="cont">
			<h3>Cards with an image</h3>
			<p>
				You can add an image appending a div with <code class="language-css">.card-image</code> to the card. You can either use an <code>img</code> element, which will fit orizontally and take all the available space, or specify a background-image attribute and codesmith will take care of the rest, both vertically and horizontally.
			</p>
			<pre><code class="language-html">&lt;div class=&quot;card&quot;&gt;&#13;&#10;&Tab;&lt;div class=&quot;card-image&quot;&gt;&#13;&#10;&Tab;&Tab;&lt;img src=&quot;...&quot; alt=&quot;&quot;&gt;&#13;&#10;&Tab;&lt;/div&gt;&#13;&#10;&Tab;&lt;div class=&quot;card-content&quot;&gt;&#13;&#10;&Tab;&Tab;...&#13;&#10;&Tab;&lt;/div&gt;&#13;&#10;&lt;/div&gt;</code></pre>
			<div class="row">
				<div class="col-s-12 col-m-6">
					<div class="card">
						<div class="card-image">
							<img src="../assets/images/anvil.jpg" alt="">
						</div>
						<div class="card-content">
							<h3>Card with image</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
						</div>
					</div>
				</div>
				<div class="col-s-12 col-m-6">
					<div class="card horizontal">
						<div class="card-image">
							<img src="../assets/images/anvil.jpg" alt="">
						</div>
						<div class="card-content">
							<h3>Horizontal card with image</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-s-12 col-m-6">
					<div class="card">
						<div class="card-image" style="background-image: url(../assets/images/anvil.jpg);">
						</div>
						<div class="card-content">
							<h3>Card with background image</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
						</div>
					</div>
				</div>
				<div class="col-s-12 col-m-6">
					<div class="card horizontal">
						<div class="card-image" style="background-image: url(../assets/images/anvil.jpg);">
						</div>
						<div class="card-content">
							<h3>Horizontal card with background image</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
						</div>
					</div>
				</div>
			</div>
			<div class="alert alert-warning">
				<i class="material-icons">warning</i>
				<p>Images may not fit correctly in horizontal cards due to the space available. In this case, prepare an image with the right proportions, or use the background-image method.</p>
			</div>
			<div class="alert alert-info">
				<i class="material-icons">lightbulb_outline</i>
				<p>Cards with background images work perfectly with the <a href="parallax.php">parallax</a> effect.</p>
			</div>
		</section>
		<hr class="cont">
		<section class="cont">
			<h3>Active cards</h3>
			<p>Add the class <code class="language-css">.card-hover</code> to enable material-style card elevation.</p>
			<div class="row justify-content-center">
				<div class="col-s-12 col-m-6 col-l-4">
					<div class="card card-hover">
						<div class="card-image" style="background-image: url(../assets/images/anvil.jpg);">
						</div>
						<div class="card-content">
							<h3>Card with hover effect</h3>
							<p>Hover the mouse to see the effect.</p>
						</div>
					</div>
				</div>
			</div>
		</section>
<?php include 'partials/footer.php'; ?>
