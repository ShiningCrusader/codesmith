<?php include 'partials/head.php'; ?>
<?php include 'partials/header.php'; ?>
        <main class="cont">
            <h1>Navbar</h1>
			<div class="label-group">
				<span class="label label-engine">Engine</span>
				<span class="label label-actions">Actions</span>
			</div>
			<p>Although the navbar doesn't require the theme file, customizing it from the engine file alone is a bit daunting. I suggest you to copy the properties from the _navbar.scss file and start from there if you don't want to grab the entire theme.</p>
			<p>Setting up a navbar is really easy, provided you follow this simple markup:</p>
			<pre><code class="language-html">&lt;header class=&quot;header&quot;&gt;&#13;&#10;&#x9;&lt;div class=&quot;cont&quot;&gt;&#13;&#10;&#x9;&#x9;&lt;a class=&quot;logo&quot; href=&quot;/&quot;&gt;&#13;&#10;&#x9;&#x9;&#x9;&lt;img src=&quot;...&quot; alt=&quot;&quot;&gt;&#13;&#10;&#x9;&#x9;&lt;/a&gt;&#13;&#10;&#x9;&#x9;&lt;div class=&quot;nav-toggle nav-break-m&quot; data-nav=&quot;#nav&quot;&gt;&#13;&#10;&#x9;&#x9;&#x9;&lt;i class=&quot;...&quot;&gt;Menu icon&lt;/i&gt;&#13;&#10;&#x9;&#x9;&lt;/div&gt;&#13;&#10;&#x9;&#x9;&lt;nav id=&quot;nav&quot; class=&quot;nav nav-break-m&quot;&gt;&#13;&#10;&#x9;&#x9;&#x9;&lt;ul&gt;&#13;&#10;&#x9;&#x9;&#x9;&#x9;&lt;li class=&quot;nav-item&quot;&gt;&lt;a href=&quot;...&quot;&gt;Navbar item&lt;/a&gt;&lt;/li&gt;&#13;&#10;&#x9;&#x9;&#x9;&#x9;&lt;li class=&quot;nav-item&quot;&gt;&#13;&#10;&#x9;&#x9;&#x9;&#x9;&#x9;&lt;a class=&quot;nav-sub-toggle&quot; href=&quot;#&quot;&gt;Navbar item with submenu&lt;/a&gt;&#13;&#10;&#x9;&#x9;&#x9;&#x9;&#x9;&lt;ul class=&quot;nav-sub&quot;&gt;&#13;&#10;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&lt;li class=&quot;nav-item&quot;&gt;&lt;a href=&quot;...&quot;&gt;Sub item&lt;/a&gt;&lt;/li&gt;&#13;&#10;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&lt;li class=&quot;nav-item&quot;&gt;&lt;a href=&quot;...&quot;&gt;Sub item&lt;/a&gt;&lt;/li&gt;&#13;&#10;&#x9;&#x9;&#x9;&#x9;&#x9;&#x9;&lt;li class=&quot;nav-item&quot;&gt;&lt;a href=&quot;...&quot;&gt;Sub item&lt;/a&gt;&lt;/li&gt;&#13;&#10;&#x9;&#x9;&#x9;&#x9;&#x9;&lt;/ul&gt;&#13;&#10;&#x9;&#x9;&#x9;&#x9;&lt;/li&gt;&#13;&#10;&#x9;&#x9;&#x9;&lt;/ul&gt;&#13;&#10;&#x9;&#x9;&lt;/nav&gt;&#13;&#10;&#x9;&lt;/div&gt;&#13;&#10;&lt;/header&gt;</code></pre>
			<p>To allow multiple navs, you must specify in the <code class="language-html">data-nav=""</code> the query selector of the nav you wish to trigger with the toggle button.</p>
			<p><code class="language-html">&lt;header&gt;</code> and <code class="language-html">&lt;nav&gt;</code> elements can be changed to divs, since the framework needs <code class="language-css">.header</code> and <code class="language-css">.nav</code> respectively to work.</p>
			<p>The <code class="language-css">.cont</code> element is not required and can be changed to any other container class.</p>
			<p>The <code class="language-css">.nav-break-*</code> classes are needed to specify at which breakpoint the navbar should switch from its mobile version to the desktop one. If you don't specify a class, the navbar will always appear collapsed, whereas if you use <code class="language-css">.nav-break-s</code> the navbar will be always expanded.</p>
        </main>
		<hr class="cont">
		<section class="cont">
			<h3>Moving elements</h3>
			<p>The header is a flex element and thus accepts basically anything you need to display inside of it. Furthermore, the order in which you place elements in the header is important and you can customize it further with <a href="engine_helpers.php">helper classes</a>. For example, you can use <code class="language-css">.justify-content-between</code> to push elements to the sides of the header.<br>This documentation will show you how to display a logo, a navbar and a button to toggle the navbar on small devices.</p>
			<h4>Logo</h4>
			<p>There are no special classes to position the logo. You just need to give the <code class="language-css">.logo</code> class to the anchor wrapping your image, or to the image itself if you're not linking it to a page.</p>
			<div class="alert alert-info">
				<i class="material-icons">lightbulb_outline</i>
				<p>You can give the <code class="language-css">.logo</code> class to other images if you have more than one brand to display.</p>
			</div>
			<h4>Navbar</h4>
			<p>
				The default navbar displays as a drop-down menu on mobile devices and horizontally on larger devices.
			</p>
			<p>If you want to position the navbar to the left of the logo add the <code class="language-css">.left</code> class or <code class="language-css">.right</code> to move it to the right. These two classes also affect the positioning of sub-menus when opened on larger devices.
			</p>
			<p>If you want your navbar to display vertically, sliding in from the side, add the <code class="language-css">.vertical</code> class. <code class="language-css">.left</code> and <code class="language-css">.right</code> also affect what side of the screen the navbar comes from.</p>
			<h4>Navbar toggle</h4>
			<p>As you can see from the markup, it's just a div containing an icon. You can replace the <code class="language-html">&lt;i&gt;</code> tag with an image and position the toggle with <code class="language-css">.left</code> and <code class="language-css">.right</code>.</p>
		</section>
<?php include 'partials/footer.php'; ?>
