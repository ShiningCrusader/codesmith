		<footer>
			<div class="cont">
				<div class="row">
					<div class="col-s-12 col-m-8">
						<p>
							Codesmith is a project by Paolo Bonacina.
						</p>
					</div>
					<div class="col-s-12 col-m-4 txt-right">
						<a href="https://twitter.com/ShiningCrusader" target="_blank">Twitter</a>
					</div>
				</div>
			</div>
		</footer>
	<script src="http://code.jquery.com/jquery-3.2.1.min.js" charset="utf-8"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.15.0/prism.min.js" charset="utf-8"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.15.0/components/prism-css.js" charset="utf-8"></script>
	<script src="../assets/js/codesmith-actions.js" charset="utf-8"></script>
	<script src="../assets/js/codesmith-plugins.js" charset="utf-8"></script>
	<script src="../assets/js/tutorial.js" charset="utf-8"></script>
	</body>
</html>
