<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>CodeSmith</title>
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/prism/1.15.0/themes/prism.min.css">
		<link rel="stylesheet" href="../assets/css/codesmith-engine.css">
		<link rel="stylesheet" href="../assets/css/codesmith-theme.css">
		<link rel="stylesheet" href="../assets/css/tutorial.css">
		<link rel="shortcut icon" href="../assets/images/icon.png">
	</head>
	<body class="header-space">
