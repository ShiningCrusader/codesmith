<?php include 'partials/head.php'; ?>
<?php include 'partials/header.php'; ?>
        <main class="cont">
			<h1>Panels</h1>
			<div class="label-group">
				<span class="label label-theme">Theme</span>
			</div>
			<p>
				Panels act as containers to separate the content of pages and can be easily customized with colors.
			</p>
			<div class="clear-both"></div>
			<div class="panel">
				<div class="panel-title">
					<h4>Panel title</h4>
				</div>
				<div class="panel-content">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
				</div>
			</div>
			<pre><code class="language-html">&lt;div class=&quot;panel&quot;&gt;&#13;&#10;&Tab;&lt;div class=&quot;panel-title&quot;&gt;&#13;&#10;&Tab;&Tab;&lt;h4&gt;Panel title&lt;/h4&gt;&#13;&#10;&Tab;&lt;/div&gt;&#13;&#10;&Tab;&lt;div class=&quot;panel-content&quot;&gt;&#13;&#10;&Tab;&Tab;...&#13;&#10;&Tab;&lt;/div&gt;&#13;&#10;&lt;/div&gt;</code></pre>
	    </main>
		<hr class="cont">
		<section class="cont">
			<h3>Colors</h3>
			<div class="label-group">
				<span class="label label-theme">Theme</span>
			</div>
			<p>Panels come in 7 flavors:</p>
			<div class="clear-both"></div>
			<?php
				$flavors = ['primary', 'secondary', 'accent', 'neutral', 'info', 'success', 'error', 'warning'];
				foreach ($flavors as $_flavor):
			?>
				<div class="panel panel-<?php echo $_flavor; ?>">
					<div class="panel-title">
						<h4><?php echo ucfirst($_flavor); ?></h4>
					</div>
					<div class="panel-content">
						This is a panel with <code class="language-css">.panel-<?php echo $_flavor; ?></code>
					</div>
				</div>
			<?php endforeach; ?>
			<p><code class="language-css">.panel-primary</code>, <code class="language-css">.panel-secondary</code> and <code class="language-css">.panel-accent</code> are dependant on this theme and their look may vary a lot when you configure a new theme.</p>
		</section>
<?php include 'partials/footer.php'; ?>
