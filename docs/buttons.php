<?php include 'partials/head.php'; ?>
<?php include 'partials/header.php'; ?>
        <main class="cont">
			<h1>Buttons</h1>
			<div class="label-group">
				<span class="label label-theme">Theme</span>
			</div>
			<p>The basic syntax is the following:</p>
			<div class="clear-both"></div>
			<pre><code class="language-html">&lt;a class="btn" href=""&gt;Click me!&lt;/a&gt;</code></pre>
			<p>
				<a class="btn">
					Click me!
				</a>
			</p>
			<p>The default button uses shades derived from the background color to make itself look pretty, but you might want to change that with color classes.</p>
		</main>
		<hr class="cont">
		<section class="cont">
			<h3>Colors</h3>
			<p>Buttons come in 7 flavors:</p>
			<div class="mb1e">
				<?php
					$flavors = ['primary', 'secondary', 'accent', 'neutral', 'info', 'success', 'error', 'warning'];
					foreach ($flavors as $_flavor):
				?>
								<a class="btn btn-<?php echo $_flavor; ?>">
									.btn-<?php echo $_flavor; ?>
								</a>
				<?php endforeach; ?>
			</div>
			<p><code class="language-css">.btn-primary</code> and <code class="language-css">.btn-secondary</code> are dependant on this theme and their look may vary a lot when you configure a new theme.</p>
			<h3>Icons</h3>
			<p>You can easily add an <code>i</code> element and it will be centered vertically. You might want to add helper classes to add a little space when there are both text and icons.</p>
			<a class="btn btn-info">
				<i class="material-icons mr5p">message</i>Button with icon
			</a>
			<h3>Wide buttons</h3>
			<p>Adding the <code class="language-css">.btn-block</code> will force the button to occupy the whole width of its container.</p>
			<div class="btn btn-info btn-block">Block button</div>
			<h3>Button groups</h3>
			<p>
				You can group buttons using a div with the <code class="language-css">.btn-group</code> class. If you need to display them vertically, just add <code class="language-css">.vertical</code> as well.
			</p>
			<div class="btn-group mb1e">
				<a href="" class="btn">Button 1</a>
				<a href="" class="btn">Button 2</a>
				<a href="" class="btn">Button 3</a>
			</div>
			<div class="btn-group vertical mb1e">
				<a href="" class="btn">Button 1</a>
				<a href="" class="btn">Button 2</a>
				<a href="" class="btn">Button 3</a>
			</div>
		</section>
<?php include 'partials/footer.php'; ?>
