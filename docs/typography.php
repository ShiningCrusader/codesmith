<?php include 'partials/head.php'; ?>
<?php include 'partials/header.php'; ?>
<main class="cont">
	<h1>Typography</h1>
	<p>Basic thypography rules for readability are defined in the engine, while colors and specific font settings are defined in the theme.</p>
</main>
<hr class="cont">
<section class="cont">
	<div class="label-group">
		<span class="label label-engine">Engine</span>
		<span class="label label-theme">Theme</span>
	</div>
	<p>
		All headers that are the first child of their parent don't have a top margin so they look good whether they are between paragraphs or not. The theme also eliminates any margin between header pairings like <code class="language-css">h1 + h2</code> or <code class="language-css">h3 + h4</code>.
	</p>
	<p>I hope you will forgive me for using 2 <code class="language-css">h1</code> on the same page, this time.</p>
	<?php for ($i=1; $i < 7; $i++): ?>
		<h<?php echo $i ?>>Heading <?php echo $i ?></h<?php echo $i ?>>
	<?php endfor; ?>
</section>
<hr class="cont">
<section class="cont">
	<div class="label-group">
		<span class="label label-engine">Engine</span>
	</div>
	<p>This is a paragraph Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
</section>
<hr class="cont">
<section class="cont">
	<h3>Lists</h3>
	<div class="label-group">
		<span class="label label-engine">Engine</span>
	</div>
	<ul>
		<li>Unordered list item 1</li>
		<li>Unordered list item 2</li>
		<li>Unordered list item 3</li>
	</ul>
	<ol>
		<li>Ordered list item 1</li>
		<li>Ordered list item 2</li>
		<li>Ordered list item 3</li>
	</ol>
	<ul class="iconized">
		<li><i class="material-icons">favorite</i>Iconized list item 1</li>
		<li><i class="material-icons">star</i>Iconized list item 2</li>
		<li><i class="material-icons">thumb_up</i>Iconized list item 3</li>
	</ul>
	<p>The <code class="language-css">.iconized</code> class allows you to use icons in place of the usual list style. It requires an icon set that uses the <code>i</code> element or an <code>img</code>.</p>
	<p>The <code class="language-css">.unstyled</code> class removes bullet points and numbering.</p>
</section>
<hr class="cont">
<section class="cont">
	<h3>Code</h3>
	<div class="label-group">
		<span class="label label-theme">Theme</span>
	</div>
	<p>Text wrapped in <code class="language-css">code</code> tags appears this way:</p>
	<p>
		<code>codesmith.isAwesome = true;</code>
	</p>
	<p>While <code class="language-css">pre</code> blocks of code have their own style.</p>
	<pre>// I'm a long bunch of code

if (codesmith.isAwesome && you.knowIt) {
	hands.clap();
}</pre>
<div class="alert alert-icon alert-warning">
	<i class="material-icons">warning</i>
	<p>
	This documentation uses Prism <a href="https://prismjs.com/index.html" target="_blank"><i class="material-icons">link</i></a> for syntax highlighting. The styles above correctly represent how <code class="language-css">pre</code> and <code class="language-css">code</code> tags appear in default Codesmith.
	</p>
</div>
</section>
<hr class="cont">
<section class="cont">
	<h3>Marks</h3>
	<div class="label-group">
		<span class="label label-theme">Theme</span>
	</div>
	<h4>This title has a <mark>marked</mark> word</h4>
	<p>This paragraph has a <mark>marked</mark> word</p>
</section>
<hr class="cont">
<section class="cont">
	<h3>Quotations</h3>
	<h4>Quote</h4>
	<q cite="http">Lorem Ipsum</q>
	<h4>Blockquote</h4>
	<blockquote cite="http://">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</blockquote>
	<h4>Abbreviation</h4>
	<abbr title="Lorem Ipsum">LI</abbr>
	<h4>Citation</h4>
	<cite>Lorem Ipsum</cite>
</section>
<?php include 'partials/footer.php'; ?>
