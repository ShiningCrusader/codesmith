<?php include 'partials/head.php'; ?>
<?php include 'partials/header.php'; ?>
        <main class="cont">
            <h1>Toggables</h1>
			<p>Toggables can help you save space and show or hide content on the fly.</p>
        </main>
		<hr class="cont">
		<section class="cont">
			<h3>Toggable</h3>
			<div class="label-group">
				<span class="label label-engine">Engine</span>
				<span class="label label-actions">Actions</span>
			</div>
			<p>A toggable is just an element that opens a div on click.</p>
			<div class="clear-both"></div>
			<p><a href="#" class="btn" data-toggable="#toggable">Open toggable</a></p>
			<div id="toggable" class="toggable">
				Toggable content.
			</div>
			<pre><code class="language-html">&lt;a href=&quot;#&quot; class=&quot;btn btn-primary&quot; data-panel=&quot;#toggable&quot;&gt;&#13;&#10;&Tab;Open toggable&#13;&#10;&lt;/a&gt;&#13;&#10;&lt;div id=&quot;toggable&quot; class=&quot;toggable&quot;&gt;&#13;&#10;&Tab;Toggable content.&#13;&#10;&lt;/div&gt;</code></pre>
			<p>If you also add a <code class="language-css">[data-parent]</code> attribute your element will also trigger all toggables that share the same parent element, closing them automatically when another toggable is triggered.</p>
			<div id="toggable-group" class="mb1e">
				<?php for ($i=1; $i <= 3; $i++): ?>
					<div>
						<a href="#" class="btn" data-toggable="#toggable-<?php echo $i; ?>" data-parent="#toggable-group">
							Toggable <?php echo $i; ?>
						</a>
						<div id="toggable-<?php echo $i; ?>" class="toggable slide">Toggable <?php echo $i; ?> content.</div>
					</div>
				<?php endfor; ?>
			</div>
			<pre><code class="language-html">&lt;div id=&quot;toggable-group&quot;&gt;&#13;&#10;&Tab;&lt;div&gt;&#13;&#10;&Tab;&Tab;&lt;a href=&quot;#&quot; class=&quot;btn btn-primary&quot; data-toggable=&quot;#toggable-1&quot; data-parent=&quot;#toggable-group&quot;&gt;&#13;&#10;&Tab;&Tab;&Tab;Toggable 1&#13;&#10;&Tab;&Tab;&lt;/a&gt;&#13;&#10;&Tab;&Tab;&lt;div id=&quot;toggable-1&quot; class=&quot;toggable&quot;&gt;Toggable 1 content.&lt;/div&gt;&#13;&#10;&Tab;&lt;/div&gt;&#13;&#10;&Tab;&lt;div&gt;&#13;&#10;&Tab;&Tab;&lt;a href=&quot;#&quot; class=&quot;btn btn-primary&quot; data-toggable=&quot;#toggable-2&quot; data-parent=&quot;#toggable-group&quot;&gt;&#13;&#10;&Tab;&Tab;&Tab;Toggable 2&#13;&#10;&Tab;&Tab;&lt;/a&gt;&#13;&#10;&Tab;&Tab;&lt;div id=&quot;toggable-2&quot; class=&quot;toggable&quot;&gt;Toggable 2 content.&lt;/div&gt;&#13;&#10;&Tab;&lt;/div&gt;&#13;&#10;&Tab;&lt;div&gt;&#13;&#10;&Tab;&Tab;&lt;a href=&quot;#&quot; class=&quot;btn btn-primary&quot; data-toggable=&quot;#toggable-3&quot; data-parent=&quot;#toggable-group&quot;&gt;&#13;&#10;&Tab;&Tab;&Tab;Toggable 3&#13;&#10;&Tab;&Tab;&lt;/a&gt;&#13;&#10;&Tab;&Tab;&lt;div id=&quot;toggable-3&quot; class=&quot;toggable&quot;&gt;Toggable 3 content.&lt;/div&gt;&#13;&#10;&Tab;&lt;/div&gt;&#13;&#10;&lt;/div&gt;</code></pre>
		</section>
		<hr class="cont">
		<section class="cont">
			<h3>Tabs</h3>
			<div class="label-group">
				<span class="label label-engine">Engine</span>
				<span class="label label-actions">Actions</span>
			</div>
			<p>Tabs have a similar markup and work just as easily.<br>Just remember to put the <code class="language-css">.open</code> class to a tab or you won't see any content on page load.</p>
			<p>If you want your content to fade in and out, add the <code class="language-css">.fade</code> class to the <code class="language-css">.tab-group</code> element</p>
			<div class="btn-group">
				<?php for ($i=1; $i <= 3; $i++): ?>
					<a class="btn" data-tab="#tab-<?php echo $i; ?>" data-parent="#tab-group">
						Tab item <?php echo $i; ?>
					</a>
				<?php endfor; ?>
			</div>
			<div id="tab-group" class="fade">
				<?php for ($i=1; $i <= 3; $i++): ?>
					<div id="tab-<?php echo $i; ?>" class="toggable <?php echo ($i == 1)? 'open': ''; ?>">
						Tab content <?php echo $i; ?>. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
					</div>
				<?php endfor; ?>
			</div>
			<div class="alert alert-warning">
				<i class="material-icons">warning</i>
				<p>The tag <code class="language-css">[data-parent]</code> is required for tabs.</p>
			</div>
			<pre><code class="language-html">&lt;div class=&quot;btn-group&quot;&gt;&#13;&#10;&Tab;&lt;a class=&quot;btn&quot; data-tab=&quot;#tab-1&quot; data-parent=&quot;#tab-group&quot;&gt;&#13;&#10;&Tab;&Tab;Tab item 1&#13;&#10;&Tab;&lt;/a&gt;&#13;&#10;&Tab;&lt;a class=&quot;btn&quot; data-tab=&quot;#tab-2&quot; data-parent=&quot;#tab-group&quot;&gt;&#13;&#10;&Tab;&Tab;Tab item 2&#13;&#10;&Tab;&lt;/a&gt;&#13;&#10;&Tab;&lt;a class=&quot;btn&quot; data-tab=&quot;#tab-3&quot; data-parent=&quot;#tab-group&quot;&gt;&#13;&#10;&Tab;&Tab;Tab item 3&#13;&#10;&Tab;&lt;/a&gt;&#13;&#10;&lt;/div&gt;&#13;&#10;&lt;div id=&quot;tab-group&quot; class=&quot;fade&quot;&gt;&#13;&#10;&Tab;&lt;div id=&quot;tab-1&quot; class=&quot;toggable open&quot;&gt;&#13;&#10;&Tab;&Tab;Tab content 1. Lorem ipsum dolor sit amet...&#13;&#10;&Tab;&lt;/div&gt;&#13;&#10;&Tab;&lt;div id=&quot;tab-2&quot; class=&quot;toggable &quot;&gt;&#13;&#10;&Tab;&Tab;Tab content 2. Lorem ipsum dolor sit amet...&#13;&#10;&Tab;&lt;/div&gt;&#13;&#10;&Tab;&lt;div id=&quot;tab-3&quot; class=&quot;toggable &quot;&gt;&#13;&#10;&Tab;&Tab;Tab content 3. Lorem ipsum dolor sit amet...&#13;&#10;&Tab;&lt;/div&gt;&#13;&#10;&lt;/div&gt;</code></pre>
		</section>
		<hr class="cont">
		<section class="cont">
			<h3>Toggler</h3>
			<div class="label-group">
				<span class="label label-actions">Actions</span>
			</div>
			<p>The toggler is the most generic element you can get. Its purpose is that of toggling the classes of an element, or list of elements.</p>
			<p>To create a toggler you need to add <code class="language-css">[data-toggle="..."]</code> to any element. The content of the data attribute is the list of classes you wish to toggle.</p>
			<p>To tell the toggler which element should be toggled, you have specify a jquery selector in the <code class="language-css">[href=""]</code> attribute or a <code class="language-css">[data-target=""]</code> attribute. If neither is present, the toggler will toggle classes on itself.</p>
			<div class="mb1e">
				<a href="#toggle1" class="btn" data-toggle="none">Click me to show an alert</a>
				<div id="toggle1" class="alert alert-success none">Here I am!</div>
			</div>
			<div class="mb1e">
				<a class="btn" data-toggle="btn-success">Click me to change colors</a>
			</div>
			<p>You can use this in really creative ways if combined with css animations.</p>
		</section>
<?php include 'partials/footer.php'; ?>
