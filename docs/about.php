<?php include 'partials/head.php'; ?>
<?php include 'partials/header.php'; ?>
        <main class="cont">
            <h1>About</h1>
			<p>Tired of aligning everything yourself or using libraries that require you to override tons of properties for your site to look like you want it?</p>
			<p>Codesmith is a mobile-first framework heavily based on flexbox that aims to minimize the code you need to write and keep customization easy.</p>
			<p>Just look at this page's code. Ain't it pretty?</p>
        </main>
		<hr class="cont">
		<section class="cont">
			<h3>Engine and themes</h3>
			<p>Instead of giving you an all-in-one package with tons of styles you probably don't need, I designed Codesmith split into two halves: engine and theme.</p>
			<p>Codesmith Engine contains everything you need to make your site work, without any significant visual style attached, while Codesmith Theme, on the other hand, needs the Engine to work and adds the essential styles to make your site look good from the get-go, without excessive frills. You'll probably need to customize your site further and I certainly don't want to hinder you in that.</p>
			<p>Two more files, Actions and Plugins are little javascript snippets that add functionality and animation to otherwise static css components. Codesmith Actions contains the javascript required for many basic components that are found in the Engine while Codesmith Plugins contains javascript functions that add a little flair to your pages.</p>
			<p>Reading the docs you'll find these three labels pointing out what file you need to include to use the features you're reading about.</p>
			<span class="label label-engine">Engine</span>
			<span class="label label-theme">Theme</span>
			<span class="label label-actions">Actions</span>
			<span class="label label-plugins">Plugins</span>
		</section>
		<hr class="cont">
		<section class="cont">
			<h3>Responsive and adaptive</h3>
			<p>
				Like other frameworks before it, Codesmith is mobile-first. This means that you'll have a much easier time designing your templates and content for smaller devices and moving or adding content for larger devices.
			</p>
			<p>
				Codesmith Engine handles different devices at three screen-width breakpoints: 768px, 1200px and 1600px;
			</p>
			<p>
				Margins and paddings are calculated from window size or font size, keeping everything visually balanced regardless of device or user zoom.
			</p>
		</section>
		<hr class="cont">
		<section class="cont">
			<h3>Icons</h3>
			<p>In icon examples I'm using Google's <a href="https://material.io/icons/" target="_blank">Material Icons</a> but you can easily replace it with other icon fonts like <a href="http://fontawesome.io/" target="_blank">Font Awesome</a> or the whole <a href="http://fontello.com/">Fontello</a> collection.
			</p>
		</section>
		<hr class="cont">
		<section class="cont">
			<h3>Credits</h3>
			<p>Codesmith's logo font is Abel by MADType <a href="http://www.madtype.com"><i class="material-icons" target="_blank">link</i></a></p>
			<p>The letter "C" in Codesmith's logo is from Portico by Mehmet Reha Tugcu <a href="https://www.behance.net/gallery/32934043/Portico"><i class="material-icons" target="_blank">link</i></a></p>
			<p>Pretty file input by Osvaldas Valutis <a href="https://tympanus.net/codrops/2015/09/15/styling-customizing-file-inputs-smart-way/"><i class="material-icons" target="_blank">link</i></a></p>
		</section>
<?php include 'partials/footer.php'; ?>
