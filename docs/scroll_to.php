<?php include 'partials/head.php'; ?>
<?php include 'partials/header.php'; ?>
        <main class="cont">
            <h1>Scroll to</h1>
			<div class="label-group">
				<span class="label label-plugins">Plugins</span>
			</div>
			<p>This javascript action will scroll the window to any given html element.</p>
        </main>
		<hr class="cont">
		<section class="cont">
			<h3>Usage</h3>
			<p>Add the <code class="language-css">[data-scroll=""]</code> attribute to an element and set the value of the attribute with the jquery selector of the element you want to scroll to.</p>
			<p>By default, this action takes the height of the fixed header into account. If you don't want this, either because you're using a transparent header or scrolling to a window-tall element, you can add the <code class="language-css">[data-no-header]</code> attribute.</p>
			<div id="first" class="border-2 border-primary pf2e" style="height: 50vh;">
				<h5>First block</h5>
				<a href="#" class="btn btn-info" data-scroll="#second">Scroll to second block</a>
			</div>
			<div id="second" class="border-2 border-secondary pf2e" style="height: 50vh;">
				<h5>Second block</h5>
				<a href="#" class="btn btn-info" data-scroll="#third">Scroll to third block</a>
			</div>
			<div id="third" class="border-2 border-accent pf2e" style="height: 50vh;">
				<h5>Third block</h5>
				<a href="#" class="btn btn-info" data-scroll="#first">Scroll to first block</a>
			</div>
		</section>
<?php include 'partials/footer.php'; ?>
