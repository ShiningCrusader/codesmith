<?php include 'partials/head.php'; ?>
<?php include 'partials/header.php'; ?>
<main class="cont">
	<h1>The grid system</h1>
	<p>
		As previously stated, Codesmith is heavily dependant on flexboxes and grids and this save you some alignment-related headaches, leaving the the dirty work for the browser. This will help you cover most scenarios, but if you need special positioning or different behaviors, try looking into <a href="helpers.php">helper classes</a> before writing petty code.
	</p>
	<div class="alert alert-icon alert-warning">
		<i class="material-icons">warning</i>
		<p>
		<code class="language-css">::before</code> and <code class="language-css">::after</code> pseudo-elements can interfere with the default flexbox behavior.<br>Instead of arrogantly hiding them, I'll let you handle them any way you seem fit. Just keep an eye on them in case something looks out of place.
		</p>
	</div>
</main>
<hr class="cont">
<section>
	<div class="cont">
		<h3>Containers</h3>
		<div class="label-group">
			<span class="label label-engine">Engine</span>
		</div>
		<p>
			Codesmith uses 2 containers: <code class="language-css">.cont</code> and <code class="language-css">.cont-full</code>
		</p>
		<p>
			<code class="language-css">.cont</code> keeps a fixed size which increases as screen width increases. Breakpoints are as follows:
		</p>
		<ul>
			<li>Up to 767px, the size is <b>Small</b> and <code class="language-css">.cont</code> will use all available space.</li>
			<li>From 768px to 1199px it's <b>Medium</b>.</li>
			<li>From 1200px to 1599px it's <b>Large</b>.</li>
			<li>From 1600px and beyond it's <b>Huge</b>.</li>
		</ul>
		<p>
			<code class="language-css">.cont-full</code> behaves like a full-width container all the time useful for full-page layouts.
		</p>
		<div class="label-group">
			<span class="label label-theme">Theme</span>
		</div>
		<p>
			Containers leave a little space on the side to avoid content reaching the edges of the page and falling everywhere. By default this is set at 10px but adding <code class="language-css">-exp</code> (expanded) and <code class="language-css">-con</code> (condensed) to container classes, you can double or halve this value, rispectively.
		</p>
		<div class="alert alert-icon alert-info">
			<i class="material-icons">lightbulb_outline</i>
			<p>
				While the engine files account for the existance of <code class="language-css">-exp</code> and <code class="language-css">-con</code> variants, they do not provide any padding so you'll have to set it yourself if you don't intend to use the base theme.
			</p>
		</div>
	</div>
	<div class="cont"><div class="tut-block">.cont</div></div>
	<div class="cont-exp"><div class="tut-block">.cont-exp</div></div>
	<div class="cont-con"><div class="tut-block">.cont-con</div></div>
	<div class="cont-full"><div class="tut-block">.cont-full</div></div>
	<div class="cont-full-exp"><div class="tut-block">.cont-full-exp</div></div>
	<div class="cont-full-con"><div class="tut-block">.cont-full-con</div></div>
</section>
<hr class="cont">
<section class="cont">
	<h3>Rows and cols</h3>
	<div class="label-group">
		<span class="label label-engine">Engine</span>
	</div>
	<p>Codesmith's grid system uses the 12-columns layout everyone loves. Without a theme, rows and columns have no padding and no margin.</p>
	<div class="clear-both"></div>
	<div class="row">
		<?php for ($i=0; $i < 6; $i++): ?>
			<div class="col-s-12 col-m-6 col-l-3 col-h-2">
				<div class="tut-block">col-s-12 col-m-6 col-l-3 col-h-2</div>
			</div>
		<?php endfor; ?>
	</div>
	<h4>A normal sized row</h4>
	<div class="label-group">
		<span class="label label-theme">Theme</span>
	</div>
	<p>Normally, a row <code class="language-css">.row</code> gives 20px on all sides to its child columns.</p>
	<div class="clear-both"></div>
	<div class="row">
		<div class="col-s-12 col-m-6 col-l-3"><div class="tut-block">col-s-12 col-m-6 col-l-3</div></div>
		<div class="col-s-12 col-m-6 col-l-3"><div class="tut-block">col-s-12 col-m-6 col-l-3</div></div>
		<div class="col-s-12 col-m-6 col-l-3"><div class="tut-block">col-s-12 col-m-6 col-l-3</div></div>
		<div class="col-s-12 col-m-6 col-l-3"><div class="tut-block">col-s-12 col-m-6 col-l-3</div></div>
	</div>
	<h4>A small sized row</h4>
	<div class="label-group">
		<span class="label label-theme">Theme</span>
	</div>
	<p>A compressed row <code class="language-css">.row-con</code> gives half spacing: 10px.</p>
	<div class="clear-both"></div>
	<div class="row-con">
		<div class="col-s-12 col-m-6 col-l-3"><div class="tut-block">col-s-12 col-m-6 col-l-3</div></div>
		<div class="col-s-12 col-m-6 col-l-3"><div class="tut-block">col-s-12 col-m-6 col-l-3</div></div>
		<div class="col-s-12 col-m-6 col-l-3"><div class="tut-block">col-s-12 col-m-6 col-l-3</div></div>
		<div class="col-s-12 col-m-6 col-l-3"><div class="tut-block">col-s-12 col-m-6 col-l-3</div></div>
	</div>
	<h4>A large sized row</h4>
	<div class="label-group">
		<span class="label label-theme">Theme</span>
	</div>
	<p>While an expanded row <code class="language-css">.row-exp</code> gives twice the space: 40px.</p>
	<div class="clear-both"></div>
	<div class="row-exp">
		<div class="col-s-12 col-m-6 col-l-3"><div class="tut-block">col-s-12 col-m-6 col-l-3</div></div>
		<div class="col-s-12 col-m-6 col-l-3"><div class="tut-block">col-s-12 col-m-6 col-l-3</div></div>
		<div class="col-s-12 col-m-6 col-l-3"><div class="tut-block">col-s-12 col-m-6 col-l-3</div></div>
		<div class="col-s-12 col-m-6 col-l-3"><div class="tut-block">col-s-12 col-m-6 col-l-3</div></div>
	</div>
</section>
<hr class="cont">
<section class="cont">
	<h3>Grids</h3>
	<div class="label-group">
		<span class="label label-engine">Engine</span>
	</div>
	<p>Codesmith's grid system also allows the use of CSS Grids. By default, grids use the same 12-columns layout, but it allows you to set an arbitrary column count.</p>
	<p>Grids also handle bottom margins automatically, evenly spacing your contents. The classes for the columns are the same for the rows.</p>
	<div class="grid">
		<?php for ($i=0; $i < 6; $i++): ?>
			<div class="col-s-12 col-m-6 col-l-3 col-h-2">
				<div class="tut-block">col-s-12 col-m-6 col-l-3 col-h-2</div>
			</div>
		<?php endfor; ?>
	</div>
	<h4>A normal sized grid</h4>
	<div class="label-group">
		<span class="label label-theme">Theme</span>
	</div>
	<p>Normally, a grid <code class="language-css">.grid</code> has a 20px gap.</p>
	<div class="grid">
		<div class="col-s-12 col-m-6 col-l-3"><div class="tut-block">col-s-12 col-m-6 col-l-3</div></div>
		<div class="col-s-12 col-m-6 col-l-3"><div class="tut-block">col-s-12 col-m-6 col-l-3</div></div>
		<div class="col-s-12 col-m-6 col-l-3"><div class="tut-block">col-s-12 col-m-6 col-l-3</div></div>
		<div class="col-s-12 col-m-6 col-l-3"><div class="tut-block">col-s-12 col-m-6 col-l-3</div></div>
	</div>
	<h4>A small sized grid</h4>
	<div class="label-group">
		<span class="label label-theme">Theme</span>
	</div>
	<p>A compressed grid <code class="language-css">.grid-con</code> has half the normal gap: 10px.</p>
	<div class="clear-both"></div>
	<div class="grid-con">
		<div class="col-s-12 col-m-6 col-l-3"><div class="tut-block">col-s-12 col-m-6 col-l-3</div></div>
		<div class="col-s-12 col-m-6 col-l-3"><div class="tut-block">col-s-12 col-m-6 col-l-3</div></div>
		<div class="col-s-12 col-m-6 col-l-3"><div class="tut-block">col-s-12 col-m-6 col-l-3</div></div>
		<div class="col-s-12 col-m-6 col-l-3"><div class="tut-block">col-s-12 col-m-6 col-l-3</div></div>
	</div>
	<h4>A large sized grid</h4>
	<div class="label-group">
		<span class="label label-theme">Theme</span>
	</div>
	<p>While an expanded row <code class="language-css">.grid-exp</code> has twice the normal gap: 40px.</p>
	<div class="clear-both"></div>
	<div class="grid-exp">
		<div class="col-s-12 col-m-6 col-l-3"><div class="tut-block">col-s-12 col-m-6 col-l-3</div></div>
		<div class="col-s-12 col-m-6 col-l-3"><div class="tut-block">col-s-12 col-m-6 col-l-3</div></div>
		<div class="col-s-12 col-m-6 col-l-3"><div class="tut-block">col-s-12 col-m-6 col-l-3</div></div>
		<div class="col-s-12 col-m-6 col-l-3"><div class="tut-block">col-s-12 col-m-6 col-l-3</div></div>
	</div>
</section>
<section class="cont">
	<div class="alert alert-icon alert-info">
		<i class="material-icons">lightbulb_outline</i>
		<p>
			<code class="language-css">.cont</code>, <code class="language-css">.row</code> and <code class="language-css">.grid</code> classes are the only classes that use px units. This is because it'll be much easier for you to make adjustments knowing the exact size on all devices, compared to other units like vw and em.
		</p>
	</div>
</section>
<hr class="cont">
<section class="cont">
	<h3>Fill rows and grids</h3>
	<div class="label-group">
		<span class="label label-theme">Theme</span>
	</div>
	<p>
		Under certain circumstances, you might want to avoid the space between columns. The <code class="language-css">.row-fill</code> and <code class="language-css">.grid-fill</code> classes do just that.<br>
		<code class="language-css">.row-fill</code> and <code class="language-css">.grid-fill</code> are non-classes that works only in the theme, keeping the row component at its engine state.<br>
		Compare the following example with the rows above.
	</p>
	<div class="row-fill">
		<?php for ($i=0; $i < 6; $i++): ?>
			<div class="col-s-12 col-m-6 col-l-3 col-h-2">
				<div class="tut-block">
					col-s-12 col-m-6 col-l-3 col-h-2
				</div>
			</div>
		<?php endfor; ?>
	</div>
</section>
<hr class="cont">
<section class="cont">
	<h3>Ordering</h3>
	<div class="label-group">
		<span class="label label-engine">Engine</span>
	</div>
	<p>If you need to sort colums when working on responsive designs, there are some classes that leverage the power of CSS Grids. Watch these boxes switch places as soon as the page hits the "large" breakpoint.</p>
	<p>Order classes range from 1 to 12 for each of the 4 available width sizes.:</p>
	<div class="row">
		<div class="col-s-8 order-l-2"><div class="tut-block">col-s-8 order-l-2</div></div>
		<div class="col-s-4 order-l-1"><div class="tut-block"> col-s-4 order-l-1</div></div>
	</div>
	<div class="alert alert-icon alert-info">
		<i class="material-icons">lightbulb_outline</i>
		<p>
			<code class="language-css">.order-s-*</code> classes are technically useless from a mobile-first perspective, but they have been defined anyway in case you are designing your page differently.
		</p>
	</div>
</section>
<?php include 'partials/footer.php'; ?>
