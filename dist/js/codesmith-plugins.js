/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./assets/ts/codesmith-plugins.ts":
/*!****************************************!*\
  !*** ./assets/ts/codesmith-plugins.ts ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const parallax_1 = __webpack_require__(/*! ./plugins/parallax */ "./assets/ts/plugins/parallax.ts");
const scroll_1 = __webpack_require__(/*! ./plugins/scroll */ "./assets/ts/plugins/scroll.ts");
const reveal_1 = __webpack_require__(/*! ./plugins/reveal */ "./assets/ts/plugins/reveal.ts");
$(function () {
    $.extend(window.Codesmith, {
        parallax: new parallax_1.default(),
        scroll: new scroll_1.default(),
        reveal: new reveal_1.default()
    });
    window.Codesmith.scroll.init();
    window.Codesmith.parallax.init();
});


/***/ }),

/***/ "./assets/ts/modules/config.ts":
/*!*************************************!*\
  !*** ./assets/ts/modules/config.ts ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.default = {
    SIZE_M: window.SIZE_M || 768,
    SIZE_L: window.SIZE_L || 1200,
    SIZE_H: window.SIZE_H || 1600,
    ANIMATION_TIMING: window.ANIMATION_TIMING || 200,
    NAVBAR_TRIGGER: window.NAVBAR_TRIGGER || window.SIZE_M || 768,
    LOG_HEAD: 'Codesmith'
};


/***/ }),

/***/ "./assets/ts/modules/helpers.ts":
/*!**************************************!*\
  !*** ./assets/ts/modules/helpers.ts ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = __webpack_require__(/*! ./config */ "./assets/ts/modules/config.ts");
class Vec2 {
    constructor(x = 0, y = 0) {
        this.x = x;
        this.y = y;
    }
    length() {
        return Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2));
    }
    normalize() {
        let length = this.length();
        this.x /= length;
        this.y /= length;
    }
}
exports.Vec2 = Vec2;
class Delay {
    constructor(context, callback, time, retrigger = false) {
        this.time = null;
        this.context = context;
        this.callback = callback;
        this.retrigger = retrigger;
        this.time = time;
        this.state = Delay.STATE_PAUSED;
    }
    static get STATE_PAUSED() {
        return 0;
    }
    static get STATE_PLAYING() {
        return 1;
    }
    start() {
        if (!this.isPlaying || this.retrigger) {
            this.state = Delay.STATE_PLAYING;
            this.timeout = window.setTimeout(() => {
                this.state = Delay.STATE_PAUSED;
                this.callback.call(this.context);
            }, this.time);
        }
    }
    stop() {
        this.state = Delay.STATE_PAUSED;
        clearTimeout(this.timeout);
    }
    isPlaying() {
        return this.state;
    }
}
exports.Delay = Delay;
class Window {
    static get size() {
        let win = $(window);
        return new Vec2(win.innerWidth(), win.innerHeight());
    }
    static get scroll() {
        return $(window).scrollTop();
    }
    constructor() { }
}
exports.Window = Window;
class Query {
    constructor() { }
    static query(size) {
        return window.matchMedia('(min-width: ' + size + 'px)');
    }
    static get query_m() {
        return Query.query(config_1.default.SIZE_M).matches;
    }
    static get query_l() {
        return Query.query(config_1.default.SIZE_L).matches;
    }
    static get query_h() {
        return Query.query(config_1.default.SIZE_H).matches;
    }
    static get navbar() {
        return Query.query(config_1.default.NAVBAR_TRIGGER).matches;
    }
}
exports.Query = Query;
class Log {
    constructor() { }
    static log(text) {
        console.log(config_1.default.LOG_HEAD + ' - ' + text);
    }
    static warn(text) {
        console.warn(config_1.default.LOG_HEAD + ' - ' + text);
    }
    static error(text) {
        console.log(config_1.default.LOG_HEAD + ' - ' + text);
    }
}
exports.Log = Log;


/***/ }),

/***/ "./assets/ts/plugins/parallax.ts":
/*!***************************************!*\
  !*** ./assets/ts/plugins/parallax.ts ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
class Parallax {
    constructor() { }
    init() {
        $(window).on('load.cs.parallax scroll.cs.parallax resize.cs.parallax', () => {
            this.update();
        });
        this.update();
    }
    update() {
        let win_top = $(window).scrollTop();
        let win_height = $(window).height();
        $('[data-parallax]').each(function () {
            let elem = $(this);
            if (parseFloat(elem.data('parallax')) > 0) {
                let elem_height = elem.outerHeight();
                let elem_bottom = elem.offset().top + elem_height;
                let scroll_percentage = ((elem_bottom - win_top) / (win_height + elem_height)) * 100;
                let position = 50 + (scroll_percentage - 50) * elem.data('parallax');
                elem.css('background-position-y', position + '%');
                elem.css('background-position-x', '50%');
            }
        });
    }
}
exports.default = Parallax;


/***/ }),

/***/ "./assets/ts/plugins/reveal.ts":
/*!*************************************!*\
  !*** ./assets/ts/plugins/reveal.ts ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const helpers_1 = __webpack_require__(/*! ../modules/helpers */ "./assets/ts/modules/helpers.ts");
class Reveal {
    constructor() {
        this.defaults = {
            threshold: -50,
            delay: 150
        };
    }
    init(options) {
        options = Object.assign(Object.assign({}, this.defaults), options);
        if (!options.selector) {
            helpers_1.Log.error('Reveal - You need to provide a jQuery selector');
        }
        else {
            this.targets = $(options.selector);
            this.threshold = options.threshold;
            this.delay = new helpers_1.Delay(this, function () {
                this.trigger();
            }, options.delay, true);
            this.targets.css('animation-name', 'none');
            this.trigger();
            $(window).on('scroll.cs.reveal', () => {
                this.check();
            });
        }
    }
    check() {
        this.delay.start();
    }
    trigger() {
        let scroll = helpers_1.Window.scroll;
        let temp_targets = this.targets;
        let self = this;
        this.targets.each(function () {
            let elem = $(this);
            let elem_top = elem.offset().top;
            let elem_bottom = elem_top + elem.outerHeight();
            if (elem_top < scroll + helpers_1.Window.size.y + self.threshold && elem_bottom > scroll - self.threshold) {
                elem.attr('style', '');
                elem.css('visibility', 'visible');
                temp_targets = temp_targets.not(elem);
            }
        });
        this.targets = temp_targets;
        if (!this.targets.length) {
            $(window).off('scroll.cs.reveal');
        }
    }
}
exports.default = Reveal;


/***/ }),

/***/ "./assets/ts/plugins/scroll.ts":
/*!*************************************!*\
  !*** ./assets/ts/plugins/scroll.ts ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = __webpack_require__(/*! ../modules/config */ "./assets/ts/modules/config.ts");
class Scroll {
    constructor() { }
    init() {
        $(document).on('click', '[data-scroll]', function (e) {
            e.preventDefault();
            let elem = $(this);
            let target = $(elem.data('scroll'));
            let target_top = target.offset().top;
            let scroll_position;
            if (elem.data('no-header'))
                scroll_position = target_top;
            else {
                scroll_position = target_top - $('.header').outerHeight();
            }
            target.trigger('start.cs.scroll');
            $('html, body')
                .stop()
                .animate({ scrollTop: scroll_position }, config_1.default.ANIMATION_TIMING * 2.5, function () {
                target.trigger('finish.cs.scroll');
            });
        });
    }
}
exports.default = Scroll;


/***/ }),

/***/ 1:
/*!**********************************************!*\
  !*** multi ./assets/ts/codesmith-plugins.ts ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! D:\Wamp\www\codesmith\assets\ts\codesmith-plugins.ts */"./assets/ts/codesmith-plugins.ts");


/***/ })

/******/ });
//# sourceMappingURL=codesmith-plugins.js.map